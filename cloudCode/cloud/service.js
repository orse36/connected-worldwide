var user = require('cloud/user.js').User;
var utilities = require('cloud/utilities.js').Utilities;
var ServicesUnsubscribeRuleObject = require('cloud/ServicesUnsubscribeRuleObject.js').ServicesUnsubscribeRuleObject;
var ServicesUnsubscribeProperties = require('cloud/ServicesUnsubscribeRuleObject.js').ServicesUnsubscribeProperties;

exports.Service = function()
{

}

exports.Service.addServiceObservers = [];

exports.Service.addObserver = function(observer)
{
  exports.Service.addServiceObservers.push(observer);
}

exports.Service.removeObserver = function(observer)
{
  exports.Service.addServiceObservers.remove(observer);
}

function checkServicesNotSubscribedInPackages(userId, service, success, error)
{
  var PackageSubscription = Parse.Object.extend("PackageSubscription");
  var query = new Parse.Query(PackageSubscription);

  query
  .equalTo("user", {
    __type: "Pointer",
    className: "_User",
    objectId: userId
     })
  .find({success: function(packageSubscriptions){

    utilities.asyncLoop(packageSubscriptions.length, function(loop, index, done){

      var packageSubscription = packageSubscriptions[index];

      var packageSubscriptionServicesRelation = packageSubscription.get("package").relation("services");

      packageSubscriptionServicesRelation.query().find({
        success: function(packageSubscriptionServices){

          for (var keyPackageSubscription in packageSubscriptionServices)
          {
            var packageSubscriptionService = packageSubscriptionServices[keyPackageSubscription];

            if (packageSubscriptionService.id === service.id)
            {
              error("Service " + service.get('name') + " already subscribed");

              done.done = true;

              return;
            }
          }

          loop.next();
        },
        error: function(_error)
        {
          done.done = true;

          error(_error);
        }
      });
    },
    function(){
      success();
    });
  },
  error: function(_error){
    error(_error);
  }});
}

function addService(userId, serviceName, success, error)
{
  Parse.Cloud.useMasterKey();

  user.getUserForId(userId, function(user){
    var Service = Parse.Object.extend("Service");

    var query = new Parse.Query(Service);
    query
    .equalTo("name", serviceName)
    .first({
      //if the query is successfully done
      success: function(service) {
          //initialize the query to check if user already has this service
          var Subscription = Parse.Object.extend("Subscription");

          var querySubscription = new Parse.Query(Subscription);

          var serviceId = service.id;

          //find the exact subscription
          querySubscription
          .equalTo("service", {
            __type: "Pointer",
            className: "Service",
            objectId: serviceId
             })
          .equalTo("user", {
            __type: "Pointer",
            className: "_User",
            objectId: userId
             })
          .find({
            success: function(results) {
              //do nothing if user already has it
              if(results.length > 0){
                error("Sorry, you already have the service: " + serviceName);
              }
              else {

                checkServicesNotSubscribedInPackages(userId, service, function()
                {
                  var subscription = new Subscription();

                  subscription.set("user", {
                    __type: "Pointer",
                    className: "_User",
                    objectId: userId
                     });
                  subscription.set("service", {
                    __type: "Pointer",
                    className: "Service",
                    objectId: serviceId
                  });
                  subscription.save();

                  success();
                },
                function(_error){
                  error(_error);
                })
              }
            },
            error: function(_error) {
              error(_error.message);
            }
          });
        },
        error: function(_error) {
          error(_error.message);
        }
      });
  }, function(_error)
  {
    error(_error.message);
  });
}

exports.Service.addService = function(userId, serviceName, success, error)
{
  addService(userId, serviceName, success, error);
}

function cancelService(userId, serviceName, success, error)
{
  user.getUserForId(userId, function(user){
    //initialize the query to check if the service exists
    var Service = Parse.Object.extend("Service");
    var query = new Parse.Query(Service);
    query.equalTo("name", serviceName);
    query.find({
      success: function(results) {
        //do nothing if the service doesn't exist
        if(results.length == 0){
          error("Sorry, service doesn't exist.");
        }
        else
        {
          var serviceId = results[0].id;

          //initialize the query to check if user already has this service
          var Subscription = Parse.Object.extend("Subscription");
          var querySubscription = new Parse.Query(Subscription);
          querySubscription
          .equalTo("service", {
            __type: "Pointer",
            className: "Service",
            objectId: serviceId
             })
          .equalTo("user", {
            __type: "Pointer",
            className: "_User",
            objectId: userId
             })
          .include('user')
          .include('service')
          .find({
            success: function(results) {
              //do nothing if user doesn't even have it
              if(results.length == 0){
                error("Sorry, you haven't subscribed to this service.");
              }
              else {
                ServicesUnsubscribeRuleObject.checkRule(new ServicesUnsubscribeProperties(results[0]));
                results[0].destroy();
                success();
              }
            },
            error: function(_error) {
              error(_error.message)
            }
          });
        }
      },
      error: function(_error) {
        error(_error.message)
      }
    });
  }, function(_error)
  {
    error(_error.message);
  });
}

exports.Service.cancelService = function(userId, serviceName, success, error)
{
  cancelService(userId, serviceName, success, error);
}

exports.Service.createService = function(userId, service, success, error)
{
  user.getUserRoles(userId, function(userRoles){
    if (userRoles.length > 0 && utilities.checkUserRoles(userRoles, ["MarketingRep"]))
    {
      //initialize the query to check if the service exists
      var Service = Parse.Object.extend("Service");
      var query = new Parse.Query(Service);
      query.equalTo("name", service.sName);
      query.find({
        success: function(results) {
          if(results.length > 0) {
            error("Sorry, you can't create two services with the same name");
          }
          else{
            var newService = new Service();

            newService.set("name", service.sName);
            newService.set("duration", service.sDuration);
            newService.set("description", service.sDescription);
            newService.set("price", service.sPrice);
            newService.set("available", true);
            newService.save();

            success();
          }
        },
        error: function(_error) {
          error(_error.message);
        }
      });
    }
    else
    {
      error("You don't have the role to perform this action.");
    }
  },
  function(_error){
    error(_error.message);
  })
}

exports.Service.deleteService = function(userId, serviceName, success, error)
{
  user.getUserRoles(userId, function(userRoles){
    if (userRoles.length > 0 && utilities.checkUserRoles(userRoles, ["MarketingRep"]))
    {
      var Service = Parse.Object.extend("Service");
      var query = new Parse.Query(Service);
      query.equalTo("name", serviceName);
      query.find({
        success: function(results) {
          if (results.length == 0) {
            error("Sorry, service doesn't exist.");
          }
          else {
            var Subscription = Parse.Object.extend("Subscription");
            query = new Parse.Query(Subscription);
            query.equalTo("service", results[0]);
            query.find({
              success: function (object) {
                //delete the service if no one is having it
                if (object.length == 0) {
                  results[0].destroy();
                  success();
                }
                else {
                  results[0].set("available", false);
                  results[0].save();
                  error("You can't delete the service because there are still users using it, it has been set to unavailable.");
                }
              },
              //this is useless
              error: function (_error) {
                error(_error.message);
              }
            });
          }
        },
        error: function(_error) {
          error(_error.message);
        }
      });
    }
    else
    {
      error("You don't have the role to perform this action.");
    }
  },
  function(_error){
    error(_error.message);
  });
}

/*exports.Service.retrieveServicesCustomers = function(userId, success, error)
{
  user.getUserRoles(userId, function(userRoles){
    if (userRoles.length > 0 && utilities.checkUserRoles(userRoles, ["CustomerRep"]))
    {
      var query = new Parse.Query(Parse.User);

      query.find({
        success: function(users)
        {
          var array = [];

          utilities.asyncLoop(users.length, function(loop, index, done){
            if (user[index])
            var Subscription = Parse.Object.extend("Subscription");
            var querySubscription = new Parse.Query(Subscription);
            var tmpUserId = users[index].id;

            querySubscription
            .include("service")
            .equalTo("user", {
              __type: "Pointer",
              className: "_User",
              objectId: tmpUserId
            })
            .find({
              success: function(subscriptions){

                array[index] = {user: users[index], subscriptions: subscriptions};

                loop.next();
              },
              error: function(_error)
              {
                error(_error);
                done.done = true;
              }
            });
          }, function(){
            success(array);
          })
        },
        error: function(_error)
        {
          error(_error);
        }
      })
    }
    else
    {
      error("You don't have the role to perform this action.");
    }
  },
  function(_error){
    error(_error.message);
  });
}*/

exports.Service.addServiceToCustomer = function(currentUserId, userName, serviceName, success, error)
{
  user.getUserRoles(currentUserId, function(userRoles){
    if (userRoles.length > 0 && utilities.checkUserRoles(userRoles, ["CustomerRep"]))
    {
      var query = new Parse.Query(Parse.User);

      query
      .equalTo("username", userName)
      .first({
        success: function(user)
        {
          var userId = user.id;

          addService(userId, serviceName, success, error)
        }
      })
    }
    else
    {
      error("You don't have the role to perform this action.");
    }
  },
  function(_error){
    error(_error.message);
  });
}

exports.Service.deleteServiceToCustomer = function(currentUserId, userName, serviceName, success, error)
{
  user.getUserRoles(currentUserId, function(userRoles){
    if (userRoles.length > 0 && utilities.checkUserRoles(userRoles, ["CustomerRep"]))
    {
      var query = new Parse.Query(Parse.User);

      query
      .equalTo("username", userName)
      .first({
        success: function(user)
        {
          var userId = user.id;

          cancelService(userId, serviceName, success, error)
        }
      })
    }
    else
    {
      error("You don't have the role to perform this action.");
    }
  },
  function(_error){
    error(_error.message);
  });
}
