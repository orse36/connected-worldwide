exports.Utilities = function()
{
}

exports.Utilities.checkUserRoles = function(userRoles, neededRoles)
{
  for (var keyUser in userRoles)
  {
    var userRole = userRoles[keyUser];

    for (var keyNeeded in neededRoles)
    {
      var neededRole = neededRoles[keyNeeded];

      if (neededRole === userRole.get('name'))
      {
        return true;
      }
    }
  }

  return false;
}

exports.Utilities.asyncLoop = function(iterations, func, callback) {
    var index = 0;
    var done = {done: false};
    var loop = {
        next: function() {
            if (done.done) {
                return;
            }

            if (index < iterations) {
                func(loop, index, done);
                index++;
            }
            else {
                done.done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}
