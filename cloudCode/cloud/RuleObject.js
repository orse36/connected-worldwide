var RuleObject = function(assessor, action, result)
{
  this.assessor = assessor;

  this.action = action;
  this.result = result;
}

RuleObject.prototype.checkRule = function(properties, success, error)
{
  this.result.assessorResult = this.assessor.evaluate(properties)

  if (this.result.assessorResult === true)
  {
    this.result.actionResult = this.action.execute(properties);
  }
}

RuleObject.prototype.setAssessor = function(assessor){
  this.assessor = assessor
}

RuleObject.prototype.setAction = function(action){
  this.action = action;
}

RuleObject.prototype.setResult = function(result){
  this.result = result;
}

exports.RuleObject = RuleObject;
