var user = require('cloud/user.js').User;
var utilities = require('cloud/utilities.js').Utilities;
var PackagesUnsubscribeRuleObject = require('cloud/ServicesUnsubscribeRuleObject.js').PackagesUnsubscribeRuleObject;
var PackagesUnsubscribeProperties = require('cloud/ServicesUnsubscribeRuleObject.js').PackagesUnsubscribeProperties;

exports.Package = function()
{

}

exports.Package.addPackageObservers = [];

exports.Package.addObserver = function(observer)
{
  exports.Package.addPackageObservers.push(observer);
}

exports.Package.removeObserver = function(observer)
{
  exports.Package.addPackageObservers.remove(observer);
}

exports.Package.getSubscribedPackages = function(userId, success, error){
  var Subscription = Parse.Object.extend("PackageSubscription");
  var query = new Parse.Query(Subscription);

  query
  .equalTo("user", {
    __type: "Pointer",
    className: "_User",
    objectId: userId
     })
  .include("package")
  .find(function(subs) {

    var results = [];

    utilities.asyncLoop(subs.length, function(loop, index, done){
      var relation = subs[index].attributes.package.relation("services");

      relation.query()
      .find({
        success: function(services){
          var p = subs[index].attributes.package.attributes;
          p.services = [];
          for (var k in services){
            p.services.push(services[k].attributes);
          }
          results.push(p);
          loop.next();
        },
        error: function(err){
          done.done = true;
          error(err);
        }
      });
    }, function(){
      success(results);
    });

  }, error);
}

exports.Package.getPackages = function(success, error){
  var Package = Parse.Object.extend("Package");
  var query = new Parse.Query(Package);

  query.find({
    success: function(results){
      var pkg = [];
      utilities.asyncLoop(results.length, function(loop, index, done){
        var relation = results[index].relation("services");

        relation.query()
        .find({
          success: function(services){
            var p = results[index].attributes;
            p.services = [];
            for (var k in services){
              p.services.push(services[k].attributes);
            }
            pkg.push(p);
            loop.next();
          },
          error: function(err){
            done.done = true;
            error(err);
          }
        });
      },
      function() {
        success(pkg);
      });
    },
    error: function(err){
      error(err);
    }
  });
}

function checkServicesNotSubscribed(userId, _package, success, error)
{
  var Subscription = Parse.Object.extend("Subscription");
  var query = new Parse.Query(Subscription);

  query
  .equalTo("user", {
    __type: "Pointer",
    className: "_User",
    objectId: userId
     })
  .find({success: function(subscriptions){

    var relation = _package.relation('services');

    relation.query().find({
      success: function(packageServices){

        for (var keySubscription in subscriptions)
        {
          var subscription = subscriptions[keySubscription];

          for (var keyPackageService in packageServices)
          {
            var packageService = packageServices[keyPackageService];

            if (subscription.get("service").id === packageService.id)
            {
              error("Service " + packageService.get('name') + " already subscribed");

              return
            }
          }
        }

        success();
      },
      error: function(_error){
        error(_error);
      }
    })
  },
  error: function(_error){
    error(_error);
  }})
}

function checkPackageServicesNotSubscribed(userId, _package, success, error)
{
  var PackageSubscription = Parse.Object.extend("PackageSubscription");
  var query = new Parse.Query(PackageSubscription);

  query
  .equalTo("user", {
    __type: "Pointer",
    className: "_User",
    objectId: userId
     })
  .find({success: function(packageSubscriptions){

    var relation = _package.relation('services');

    relation.query().find({
      success: function(packageServices){

        utilities.asyncLoop(packageSubscriptions.length, function(loop, index, done){

          var packageSubscription = packageSubscriptions[index];

          var packageSubscriptionServicesRelation = packageSubscription.get("package").relation("services");

          packageSubscriptionServicesRelation.query().find({
            success: function(packageSubscriptionServices){

              for (var keyPackageSubscription in packageSubscriptionServices)
              {
                var packageSubscriptionService = packageSubscriptionServices[keyPackageSubscription];

                for (var keyPackageService in packageServices)
                {
                  var packageService = packageServices[keyPackageService];

                  if (packageSubscriptionService.id === packageService.id)
                  {
                    error("Service " + packageService.get('name') + " already subscribed");

                    done.done = true;

                    return;
                  }
                }

                loop.next();
              }
            },
            error: function(_error)
            {
              done.done = true;

              error(_error);
            }
          });
        },
        function(){
          success();
        });
      },
      error: function(_error)
      {
        error(_error);
      }
    });
  },
  error: function(_error){
    error(_error);
  }});
}

function checkAllServicesNotSubscribed(user, _package, success, error)
{
  checkServicesNotSubscribed(user, _package, function(){
    checkPackageServicesNotSubscribed(user, _package, success, error);
  },
  function(_error){
    error(_error);
  });
}

function addPackage(userId, packageName, success, error)
{
  Parse.Cloud.useMasterKey();

  user.getUserForId(userId, function(user){
    var Package = Parse.Object.extend("Package");

    var query = new Parse.Query(Package);

    query
    .equalTo("name", packageName)
    .first({
      //if the query is successfully done
      success: function(_package) {
          //initialize the query to check if user already has this package
          var PackageSubscription = Parse.Object.extend("PackageSubscription");

          var querySubscription = new Parse.Query(PackageSubscription);

          var packageId = _package.id;

          //find the exact subscription
          querySubscription
          .equalTo("package", {
            __type: "Pointer",
            className: "Package",
            objectId: packageId
             })
          .equalTo("user", {
            __type: "Pointer",
            className: "_User",
            objectId: userId
             })
          .find({
            success: function(results) {
              //do nothing if user already has it
              if(results.length > 0){
                error("Sorry, you already have the package: " + packageName);
              }
              else {

                checkAllServicesNotSubscribed(userId, _package, function(){
                  var PackageSubscription = Parse.Object.extend("PackageSubscription");
                  var subscription = new PackageSubscription();

                  subscription.set("user", {
                    __type: "Pointer",
                    className: "_User",
                    objectId: userId
                     });
                  subscription.set("package", {
                    __type: "Pointer",
                    className: "Package",
                    objectId: packageId
                  });
                  subscription.save();

                  success();
                }, function(_error){
                  error(_error);
                });
              }
            },
            error: function(_error) {
              error(_error.message);
            }
          });
        },
        error: function(_error) {

          error(_error.message);
        }
      });
  }, function(_error)
  {
    error(_error.message);
  });
}

exports.Package.addPackage = function(userId, packageName, success, error)
{
  addPackage(userId, packageName, success, error);
}

function cancelPackage(userId, packageName, success, error)
{
  user.getUserForId(userId, function(user){
    //initialize the query to check if the package exists
    var Package = Parse.Object.extend("Package");
    var query = new Parse.Query(Package);
    query.equalTo("name", packageName);
    query.find({
      success: function(results) {
        //do nothing if the package doesn't exist
        if(results.length == 0){
          error("Sorry, package doesn't exist.");
        }
        else
        {
          var packageId = results[0].id;

          //initialize the query to check if user already has this package
          var PackageSubscription = Parse.Object.extend("PackageSubscription");
          var querySubscription = new Parse.Query(PackageSubscription);
          querySubscription
          .equalTo("package", {
            __type: "Pointer",
            className: "Package",
            objectId: packageId
             })
          .equalTo("user", {
            __type: "Pointer",
            className: "_User",
            objectId: userId
             })
          .include('user')
          .include('package')
          .find({
            success: function(results) {
              //do nothing if user doesn't even have it
              if(results.length == 0){
                error("Sorry, you haven't subscribed to this package.");
              }
              else {
                results[0].get('package').relation('services').query().find({
                  success: function(servicesInPackage){
                    results[0].nbServices = servicesInPackage.length;
                    PackagesUnsubscribeRuleObject.checkRule(new PackagesUnsubscribeProperties(results[0]));
                    results[0].destroy();
                    success();
                  },
                  error: function(err){
                    error(err.message);
                  }
                });;
              }
            },
            error: function(_error) {
              error(_error.message)
            }
          });
        }
      },
      error: function(_error) {
        error(_error.message)
      }
    });
  }, function(_error)
  {
    error(_error.message);
  });
}

exports.Package.cancelPackage = function(userId, packageName, success, error)
{
  cancelPackage(userId, packageName, success, error);
}

exports.Package.createPackage = function(userId, _package, success, error)
{
  user.getUserRoles(userId, function(userRoles){
    if (userRoles.length > 0 && utilities.checkUserRoles(userRoles, ["MarketingRep"]))
    {
      //initialize the query to check if the package exists
      var Package = Parse.Object.extend("Package");
      var query = new Parse.Query(Package);
      query.equalTo("name", _package.pName);
      query.find({
        success: function(results) {
          if(results.length > 0) {
            error("Sorry, you can't create two package with the same name");
          }
          else{

            var queryServicesArray = [];
            var queryService;

            for (var key in _package.pServices)
            {
              if (_package.pServices[key] === true)
              {
                queryService = new Parse.Query(Parse.Object.extend("Service"));
                queryService.equalTo("name", key);

                queryServicesArray.push(queryService);
              }
            }
            if (queryServicesArray.length == 0){
              error("Your package need at least one service to be created.");
            }
            else {
              var queryServices = Parse.Query.or.apply(Parse.Query, queryServicesArray);

              queryServices.find({
                success: function(services) {

                  var newPackage = new Package();

                  newPackage.set("name", _package.pName);
                  newPackage.set("duration", _package.pDuration);
                  newPackage.set("description", _package.pDescription);
                  newPackage.set("price", _package.pPrice);
                  newPackage.set("available", true);

                  var relation = newPackage.relation("services");

                  var Service = Parse.Object.extend("Service");

                  for (var key in services)
                  {
                    relation.add(services[key]);
                  }

                  newPackage.save();

                  success();
                },
                error: function(_error) {
                  error(_error.message);
                }
              })
            }
          }
        },
        error: function(_error) {
          error(_error.message);
        }
      });
    }
    else
    {
      error("You don't have the role to perform this action.");
    }
  },
  function(_error){
    error(_error.message);
  })
}

exports.Package.deletePackage = function(userId, packageName, success, error)
{
  user.getUserRoles(userId, function(userRoles){
    if (userRoles.length > 0 && utilities.checkUserRoles(userRoles, ["MarketingRep"]))
    {
      var Package = Parse.Object.extend("Package");
      var query = new Parse.Query(Package);
      query.equalTo("name", packageName);
      query.find({
        success: function(results) {
          if (results.length == 0) {
            error("Sorry, package doesn't exist.");
          }
          else {
            var PackageSubscription = Parse.Object.extend("PackageSubscription");
            query = new Parse.Query(PackageSubscription);
            query.equalTo("package", results[0]);
            query.find({
              success: function (object) {
                //delete the package if no one is having it
                if (object.length == 0) {
                  results[0].destroy();
                  success();
                }
                else {
                  results[0].set("available", false);
                  results[0].save();
                  error("You can't delete the package because there are still users using it, it has been set to unavailable.");
                }
              },
              //this is useless
              error: function (_error) {
                error(_error.message);
              }
            });
          }
        },
        error: function(_error) {
          error(_error.message);
        }
      });
    }
    else
    {
      error("You don't have the role to perform this action.");
    }
  },
  function(_error){
    error(_error.message);
  });
}

/*exports.Package.retrievePackagesCustomers = function(userId, success, error)
{
  user.getUserRoles(userId, function(userRoles){
    if (userRoles.length > 0 && utilities.checkUserRoles(userRoles, ["MarketingRep"]))
    {
      var query = new Parse.Query(Parse.User);

      query.find({
        success: function(users)
        {
          var array = [];

          utilities.asyncLoop(users.length, function(loop, index, done){

            var PackageSubscription = Parse.Object.extend("PackageSubscription");
            var querySubscription = new Parse.Query(PackageSubscription);
            var tmpUserId = users[index].id;

            querySubscription
            .include("package")
            .equalTo("user", {
              __type: "Pointer",
              className: "_User",
              objectId: tmpUserId
            })
            .find({
              success: function(subscriptions){

                array[index] = {user: users[index], subscriptions: subscriptions};

                loop.next();
              },
              error: function(_error)
              {
                error(_error);
                done.done = true;
              }
            });
          }, function(){
            success(array);
          })
        },
        error: function(_error)
        {
          error(_error);
        }
      })
    }
    else
    {
      error("You don't have the role to perform this action.");
    }
  },
  function(_error){
    error(_error.message);
  });
}*/

exports.Package.addPackageToCustomer = function(currentUserId, userName, packageName, success, error)
{
  user.getUserRoles(currentUserId, function(userRoles){
    if (userRoles.length > 0 && utilities.checkUserRoles(userRoles, ["CustomerRep"]))
    {
      var query = new Parse.Query(Parse.User);

      query
      .equalTo("username", userName)
      .first({
        success: function(user)
        {
          var userId = user.id;

          addPackage(userId, packageName, success, error)
        }
      })
    }
    else
    {
      error("You don't have the role to perform this action.");
    }
  },
  function(_error){
    error(_error.message);
  });
}

exports.Package.deletePackageToCustomer = function(currentUserId, userName, packageName, success, error)
{
  user.getUserRoles(currentUserId, function(userRoles){
    if (userRoles.length > 0 && utilities.checkUserRoles(userRoles, ["CustomerRep"]))
    {
      var query = new Parse.Query(Parse.User);

      query
      .equalTo("username", userName)
      .first({
        success: function(user)
        {
          var userId = user.id;
          cancelPackage(userId, packageName, success, error)
        }
      })
    }
    else
    {
      error("You don't have the role to perform this action.");
    }
  },
  function(_error){
    error(_error.message);
  });
}

exports.Package.modifyPackageServices = function(currentUserId, packageName, serviceNames, success, error)
{
  user.getUserRoles(currentUserId, function(userRoles){
    if (userRoles.length > 0 && utilities.checkUserRoles(userRoles, ["MarketingRep"]))
    {
      //initialize the query to check if the package exists
      var Package = Parse.Object.extend("Package");
      var query = new Parse.Query(Package);
      query.equalTo("name", packageName);
      query.first({
        success: function(_package) {

          var serviceObjects = [];

          utilities.asyncLoop(serviceNames.length, function(loop, index, done){

            var queryService = new Parse.Query(Parse.Object.extend("Service"));
            queryService
            .equalTo("name", serviceNames[index])
            .first({
              success: function(service){

                serviceObjects.push(service);

                loop.next();
              },
              error: function(_error){
                error(_error);

                done.done = true;
              }
            });
          },
          function(){

            var relation = _package.relation("services");

            relation.query().find({
              success: function(packageServices){

                for (var key in packageServices)
                {
                  var packageService = packageServices[key];

                  relation.remove(packageService);
                }

                for (var key in serviceObjects)
                {
                  relation.add(serviceObjects[key]);
                }

                _package.save();

                success();
              }
            });
          });
        },
        error: function(_error) {
          error(_error.message);
        }
      });
    }
    else
    {
      error("You don't have the role to perform this action.");
    }
  },
  function(_error){
    error(_error.message);
  });
}
