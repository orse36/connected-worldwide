var RuleObject = require('cloud/RuleObject.js').RuleObject;

/*var ServicesUnsubscribeRuleObject = function(type)
{
  switch(type){
    case 'Package':
      this.assessor = new PackageUnsubscribeAssessor();
      this.action = new PackageUnsubscribeAction();
      break;
    default:
      this.assessor = new ServicesUnsubscribeAssessor();
      this.action = new ServicesUnsubscribeAction();
  }

  this.result = new ServicesUnsubscribeResult();
}

ServicesUnsubscribeRuleObject.prototype.checkRule = function(properties)
{
  this.result.assessorResult = this.assessor.evaluate(properties)

  if (this.result.assessorResult === true)
  {
    this.result.actionResult = this.action.execute(properties);
  }
}*/

var ServicesUnsubscribeProperties = function(subscription)
{
  this.service = subscription.get('service');
  this.user = subscription.get('user');
  this.subscription = subscription
}

var PackagesUnsubscribeProperties = function(subscription)
{
  this.package = subscription.get('package');
  this.user = subscription.get('user');
  this.subscription = subscription;
  this.package.nbServices = subscription.nbServices;
}

var PackageUnsubscribeAssessor = function()
{
}

PackageUnsubscribeAssessor.prototype.evaluate = function(properties)
{
  var diff = (new Date(properties.subscription.createdAt) - new Date()) / (-1000 * 60 * 60 * 24);
  return (diff < properties.package.get('duration'));
}

var PackageUnsubscribeAction = function()
{
}

PackageUnsubscribeAction.prototype.execute = function(properties)
{
  var fees = properties.user.get('fees') + (150 * properties.package.nbServices);
  properties.user.set('fees', fees);
  properties.user.save();
  return true;
}

var ServicesUnsubscribeAssessor = function()
{
}

ServicesUnsubscribeAssessor.prototype.evaluate = function(properties)
{
  var diff = (new Date(properties.subscription.createdAt) - new Date()) / (-1000 * 60 * 60 * 24);
  return (diff < properties.service.get('duration'));
}

var ServicesUnsubscribeAction = function()
{
}

ServicesUnsubscribeAction.prototype.execute = function(properties)
{
  var fees = properties.user.get('fees') + 150;
  properties.user.set('fees', fees);
  properties.user.save();
  return true;
}

var ServicesUnsubscribeResult = function()
{
  this.assessorResult = false;
  this.actionResult = false;
}

ServicesUnsubscribeResult.prototype.getAssessorResult = function()
{
  return this.assessorResult;
}

ServicesUnsubscribeResult.prototype.getActionResult = function()
{
  return this.actionResult;
}

var ServicesUnsubscribeRuleObject = new RuleObject(new ServicesUnsubscribeAssessor(), new ServicesUnsubscribeAction(), new ServicesUnsubscribeResult());
var PackagesUnsubscribeRuleObject = new RuleObject(new PackageUnsubscribeAssessor(), new PackageUnsubscribeAction(), new ServicesUnsubscribeResult());

exports.ServicesUnsubscribeRuleObject = ServicesUnsubscribeRuleObject;
exports.ServicesUnsubscribeProperties = ServicesUnsubscribeProperties;
exports.PackagesUnsubscribeRuleObject = PackagesUnsubscribeRuleObject;
exports.PackagesUnsubscribeProperties = PackagesUnsubscribeProperties;
