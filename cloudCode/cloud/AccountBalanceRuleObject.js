var userClass = require('cloud/user.js').User;
var service = require('cloud/service.js').Service;
var reqPackage = require('cloud/package.js').Package;
var RuleObject = require('cloud/RuleObject.js').RuleObject;

/*var AccountBalanceRuleObject = function()
{
  this.assessor = new AccountBalanceAssessor();

  this.action = new AccountBalanceAction();
  this.result = new AccountBalanceResult();
}

AccountBalanceRuleObject.prototype.checkRule = function(properties, success, error)
{
  this.result.assessorResult = this.assessor.evaluate(properties)

  if (this.result.assessorResult === true)
  {
    this.result.actionResult = this.action.execute(properties);
  }
}*/

var AccountBalanceProperties = function(user, subscriptions)
{
  this.user = user;
  this.subscriptions = subscriptions;
}

AccountBalanceProperties.prototype.getUser = function()
{
  return this.user;
}

AccountBalanceProperties.prototype.getSubscriptions = function()
{
  return this.subscriptions;
}

var AccountBalanceAssessor = function()
{
}

AccountBalanceAssessor.prototype.evaluate = function(properties)
{
  var count = 0;
  var subscriptions = properties.getSubscriptions();

  for (var key in subscriptions)
  {
    var subscription = subscriptions[key];

    count += subscription.get("price");
  }

  return count > properties.getUser().get("maxAccountBalance")
}

var AccountBalanceAction = function()
{
}

AccountBalanceAction.prototype.execute = function(properties)
{
  var count = 0;
  var subscriptions = properties.getSubscriptions();

  for (var key in subscriptions)
  {
    var subscription = subscriptions[key];

    count += subscription.get("price");
  }

  var body = "Your maximum account balance setted at: " + properties.getUser().get("maxAccountBalance") + " has been overflowed, your current bill (without fees) is at: " + count + " per month";

  Parse.Cloud.run("sendMail", {emailTo: properties.getUser().get("email"), subject: "[Warning] Connected Worldwide", text: body}, {
    success: function(msg) {
      console.log(msg);
    },
    error: function(error) {
      console.log(error);
    }
  });

  return true;
}

var AccountBalanceResult = function()
{
  this.assessorResult = false;
  this.actionResult = false;
}

AccountBalanceResult.prototype.getAssessorResult = function()
{
  return this.assessorResult;
}

AccountBalanceResult.prototype.getActionResult = function()
{
  return this.actionResult;
}

var AccountBalanceRuleObject = new RuleObject(new AccountBalanceAssessor(), new AccountBalanceAction(), new AccountBalanceResult());

exports.AccountBalanceRuleObject = AccountBalanceRuleObject;
exports.AccountBalanceProperties = AccountBalanceProperties;
