var loginRecord = require('cloud/loginRecord.js').LoginRecord;
var utilities = require('cloud/utilities.js').Utilities;
var UserFactory = require('cloud/UserFactory.js').UserFactory;

exports.User = function()
{
}

exports.User.signUp = function(infos, success, error)
{
    UserFactory.create('User', infos, success, error);
}

exports.User.createCR = function(infos, success, error)
{
    UserFactory.create('CustomerRep', infos, success, error);
}

exports.User.createMR = function(infos, success, error)
{
    UserFactory.create('MarketingRep', infos, success, error);
}

exports.User.logIn = function(email, password, success, error)
{
  Parse.User.logIn(email, password, {
    success: function(user){
      loginRecord.resetLoginRecord(email);
      success(user.getSessionToken());
    },
    error: function(user, err){
      loginRecord.checkLoginRecord(email);
      error(user, err);
    }
  });
}

function getUserForId(userId, success, error)
{
  Parse.Cloud.useMasterKey();

  var query = new Parse.Query(Parse.User);

  query.
  equalTo("objectId", userId)
  .first({
    success: function(user){
      success(user);
    },
    error: function(_error){
      error(_error);
    }
  });
}

exports.User.getUserForId = function(userId, success, error)
{
  getUserForId(userId, success, error);
}

exports.User.getUserRoles = function(userId, success, error)
{
  Parse.Cloud.useMasterKey();

  var queryRole = new Parse.Query(Parse.Role);

  queryRole
  .equalTo("users", {
      __type: "Pointer",
      className: "_User",
      objectId: userId
    })
  .find({success: function(roles){
      success(roles);
    },
    error: function(_errorRole){
      error(_errorRole);
    }
  });
}

exports.User.retrieveCustomers = function(userId, field, success, error)
{
  var fieldMap = {
    "service":"Subscription",
    "package":"PackageSubscription"
  };
  exports.User.getUserRoles(userId, function(userRoles){
    if (userRoles.length > 0 && utilities.checkUserRoles(userRoles, ["CustomerRep", "MarketingRep"]))
    {
      var query = new Parse.Query(Parse.Role);

      query
      .containedIn("name", ["CommercialCustomer", "RetailCustomer"])
      .find({
        success: function(roles)
        {
          var array = [];
          utilities.asyncLoop(roles.length, function(l1, idx, d){
            var relation = roles[idx].relation("users");
            relation.query()
            .find({
              success: function(users) {
                utilities.asyncLoop(users.length, function(loop, index, done){
                  var Subscription = Parse.Object.extend(fieldMap[field]);
                  var querySubscription = new Parse.Query(Subscription);
                  var tmpUserId = users[index].id;

                  querySubscription
                  .include(field)
                  .equalTo("user", {
                    __type: "Pointer",
                    className: "_User",
                    objectId: tmpUserId
                  })
                  .find({
                    success: function(subscriptions){
                      array.push({user: users[index], subscriptions: subscriptions});
                      loop.next();
                    },
                    error: function(_error){
                      error(_error);
                      done.done = true;
                    }
                  });
                }, function(){
                  l1.next();
                });
              },
              error: function (_err){
                d.done = true;
                error(_err)
              }
            });
          }, function(){
            success(array)
          });
        },
        error: function(_error)
        {
          error(_error);
        }
      })
    }
    else
    {
      error("You don't have the role to perform this action.");
    }
  },
  function(_error){
    error(_error.message);
  });
}

exports.User.changeMaxAccountBalance = function(userId, max, success, error)
{
  Parse.Cloud.useMasterKey();

  var query = new Parse.Query(Parse.User);

  getUserForId(userId, function(user){

      user.set("maxAccountBalance", max);

      user.save();

      success();
    },
    function(_error){
      error(_error);
    });
}

exports.User.getFees = function(userId, success, error){
    getUserForId(userId, function(user){
      success(user.get('fees'));
    }, function(err){
      error(err);
    });
}
