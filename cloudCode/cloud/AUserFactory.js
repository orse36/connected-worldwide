var AUserFactory = function()
{
}

AUserFactory.prototype.create = function(type, properties, success, error)
{
    if (type == 'User') {
        this.makeCustomer(properties, success, error);
    } else if (type == 'CustomerRep') {
        this.makeCustomerRep(properties, success, error);
    } else if (type == 'MarketingRep') {
        this.makeMarketingRep(properties, success, error);
    } else {
        // TODO: throw
    }
}

AUserFactory.prototype.makeCustomer = function(properties, success, error)
{
    // TODO: throw
}

AUserFactory.prototype.makeCustomerRep = function(properties, success, error)
{
    // TODO: throw
}

AUserFactory.prototype.makeMarketingRep = function(properties, success, error)
{
    // TODO: throw
}

exports.AUserFactory = AUserFactory;
