var loginRecord = require('cloud/loginRecord.js').LoginRecord;
var AUserFactory = require('cloud/AUserFactory.js').AUserFactory;

var UserFactory = new AUserFactory();

UserFactory.makeCustomer = function(properties, success, error)
{
    var user = new Parse.User();
    user.set("username", properties.username);
    user.set("password", properties.password);
    user.set("email", properties.email);
    user.set("name", properties.lastName);
    user.set("firstname", properties.firstName);
    user.set("address", properties.address);
    user.set("state", properties.state);
    user.set("zip", parseInt(properties.zip));
    user.set("city", properties.city);
    user.set("birthday", new Date(properties.birthday));
    user.set("fees", 0);

    user.signUp(null, {
        success: function(user){
            Parse.Cloud.useMasterKey();

            var query = new Parse.Query(Parse.Role);

            query.equalTo("name", (properties.accountType == 'retail') ? 'RetailCustomer' : "CommercialCustomer");
            query.first({
              success: function(object){
                object.relation("users").add(user);
                object.save();

                loginRecord.createLoginRecord(user.get('email'));

                var newACL = new Parse.ACL();

                newACL.setReadAccess(user.id, true);

                user.setACL(newACL);
                user.save();

                success(user);
              },
              error: function(err)
              {
                  error(user, err);
              }
            });
        },
        error: function(user, err){
            error(err.message);
        }
    });
}

UserFactory.makeCustomerRep = function(properties, success, error)
{
    var user = new Parse.User();
    user.set("username", properties.email);
    user.set("password", properties.password);
    user.set("email", properties.email);
    user.set("name", properties.lastName);
    user.set("firstname", properties.firstName);

    user.signUp(null, {
        success: function(user){
            Parse.Cloud.useMasterKey();

            var query = new Parse.Query(Parse.Role);

            query.equalTo("name", "CustomerRep");
            query.first({
              success: function(object){
                object.relation("users").add(user);
                object.save();

                loginRecord.createLoginRecord(user.get('email'));
                success(user);
              },
              error: function(err)
              {
                  error(user, err);
              }
            });
        },
        error: function(user, err){
            error(err.message);
        }
    });
}

UserFactory.makeMarketingRep = function(properties, success, error)
{
    var user = new Parse.User();
    user.set("username", properties.email);
    user.set("password", properties.password);
    user.set("email", properties.email);
    user.set("name", properties.lastName);
    user.set("firstname", properties.firstName);

    user.signUp(null, {
        success: function(user){
            Parse.Cloud.useMasterKey();

            var query = new Parse.Query(Parse.Role);

            query.equalTo("name", "MarketingRep");
            query.first({
              success: function(object){
                object.relation("users").add(user);
                object.save();

                loginRecord.createLoginRecord(user.get('email'));
                success(user);
              },
              error: function(err)
              {
                  error(user, err);
              }
            });
        },
        error: function(user, err){
            error(err.message);
        }
    });
}

exports.UserFactory = UserFactory;
