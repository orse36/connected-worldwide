var user = require('cloud/user.js').User;
var service = require('cloud/service.js').Service;
var reqPackage = require('cloud/package.js').Package;
var AccountBalanceObserver = require('cloud/AccountBalanceObserver.js').AccountBalanceObserver;

// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("sendMail", function(request, response) {
  var Mailgun = require('mailgun');
  Mailgun.initialize('sandbox41a5d38c303d496fad620740ef6e1afa.mailgun.org', 'key-325fbbb344c649f4d49d92b19ee142cc');

  Mailgun.sendEmail({
    to: request.params.emailTo,
    from: "ConnectedWorldwide@CloudCode.com",
    subject: request.params.subject,
    text: request.params.text
  }, {
    success: function(httpResponse) {
      console.log(httpResponse);
      response.success("Email sent!");
    },
    error: function(httpResponse) {
      console.error(httpResponse);
      response.error(httpResponse);
    }
  });
});

Parse.Cloud.define("signUp", function(request, response){
  user.signUp(request.params, response.success, response.error);
});

Parse.Cloud.define("createCR", function(request, response){
  user.createCR(request.params, response.success, response.error);
});

Parse.Cloud.define("createMR", function(request, response){
  user.createMR(request.params, response.success, response.error);
});

Parse.Cloud.define("logIn", function(request, response){
  user.logIn(request.params.email, request.params.password, response.success, response.error);
});

Parse.Cloud.define("getFees", function(request, response){
  user.getFees(request.params.userId, response.success, response.error);
});

Parse.Cloud.define("getUserRoles", function(request, response){
  user.getUserRoles(request.params.userId, response.success, response.error);
});

Parse.Cloud.define("changeMaxAccountBalance", function(request, response){
  user.changeMaxAccountBalance(request.params.userId, request.params.max, response.success, response.error);
});

Parse.Cloud.define("addService", function(request, response){
  service.addService(request.params.userId, request.params.serviceName, response.success, response.error);
});

Parse.Cloud.afterSave("Subscription", function(request){

  service.addServiceObserver = [];

  var observer = new AccountBalanceObserver(service);
  observer.subscribe();

  user.getUserForId(request.object.get("user").id, function(user){

    for (var key in service.addServiceObservers)
    {
      var tmpObserver = service.addServiceObservers[key];

      tmpObserver.notify(this, user);
    }

  }, function(error){
  })
});

Parse.Cloud.define("cancelService", function(request, response){
  service.cancelService(request.params.userId, request.params.serviceName, response.success, response.error);
});

Parse.Cloud.define("createService", function(request, response){
  service.createService(request.params.userId, request.params.service, response.success, response.error);
});

Parse.Cloud.define("deleteService", function(request, response){
  service.deleteService(request.params.userId, request.params.serviceName, response.success, response.error);
});

Parse.Cloud.define("retrieveCustomers", function(request, response){
  user.retrieveCustomers(request.params.userId, request.params.field, response.success, response.error);
})

Parse.Cloud.define("addServiceToCustomer", function(request, response){
  service.addServiceToCustomer(request.params.currentUserId, request.params.userName, request.params.serviceName, response.success, response.error);
})

Parse.Cloud.define("deleteServiceToCustomer", function(request, response){
  service.deleteServiceToCustomer(request.params.currentUserId, request.params.userName, request.params.serviceName, response.success, response.error);
})

Parse.Cloud.define("addPackage", function(request, response){
  reqPackage.addPackage(request.params.userId, request.params.packageName, response.success, response.error);
});

Parse.Cloud.afterSave("PackageSubscription", function(request){

  reqPackage.addPackageObserver = [];

  observer = new AccountBalanceObserver(reqPackage);
  observer.subscribe();

  user.getUserForId(request.object.get("user").id, function(user){

    for (var key in reqPackage.addPackageObservers)
    {
      var tmpObserver = reqPackage.addPackageObservers[key];

      tmpObserver.notify(this, user);
    }

  }, function(error){
  })
});

Parse.Cloud.define("cancelPackage", function(request, response){
  reqPackage.cancelPackage(request.params.userId, request.params.packageName, response.success, response.error);
});

Parse.Cloud.define("getSubscribedPackages", function(request, response){
  reqPackage.getSubscribedPackages(request.params.user, response.success, response.error);
});

Parse.Cloud.define("createPackage", function(request, response){
  reqPackage.createPackage(request.params.userId, request.params._package, response.success, response.error);
});

Parse.Cloud.define("getPackages", function(request, response){
  reqPackage.getPackages(response.success, response.error);
});

Parse.Cloud.define("deletePackage", function(request, response){
  reqPackage.deletePackage(request.params.userId, request.params.packageName, response.success, response.error);
});

/*Parse.Cloud.define("retrievePackagesCustomers", function(request, response){
  reqPackage.retrievePackagesCustomers(request.params.userId, response.success, response.error);
})*/

Parse.Cloud.define("addPackageToCustomer", function(request, response){
  reqPackage.addPackageToCustomer(request.params.currentUserId, request.params.userName, request.params.packageName, response.success, response.error);
})

Parse.Cloud.define("deletePackageToCustomer", function(request, response){
  reqPackage.deletePackageToCustomer(request.params.currentUserId, request.params.userName, request.params.packageName, response.success, response.error);
})

Parse.Cloud.define("modifyPackageServices", function(request, response){
  reqPackage.modifyPackageServices(request.params.currentUserId, request.params.packageName, request.params.serviceNames, response.success, response.error);
})
