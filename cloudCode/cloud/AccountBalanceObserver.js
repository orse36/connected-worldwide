var AccountBalanceRuleObject = require('cloud/AccountBalanceRuleObject.js').AccountBalanceRuleObject;
var AccountBalanceProperties= require('cloud/AccountBalanceRuleObject.js').AccountBalanceProperties;
var utilities = require('cloud/utilities.js').Utilities;

var AccountBalanceObserver = function(subject)
{
  this.subject = subject;

  this.ruleObject = AccountBalanceRuleObject;
}

AccountBalanceObserver.prototype.subscribe = function()
{
  this.subject.addObserver(this);
}

AccountBalanceObserver.prototype.unsubscribe = function()
{
  this.subject.removeObserver(this);
}

function retrieveServicesAndPackagesSubscribed(user, success, error)
{
  Parse.Cloud.useMasterKey();

  var subscriptions = [];

  var fieldMap = {
    "service":"Subscription",
    "package":"PackageSubscription"
  };

  var count = 0;

  utilities.asyncLoop(Object.keys(fieldMap).length, function(loop, index, done){
    var Subscription = Parse.Object.extend(fieldMap[Object.keys(fieldMap)[index]]);
    var querySubscription = new Parse.Query(Subscription);

    querySubscription
    .include(Object.keys(fieldMap)[index])
    .equalTo("user", {
      __type: "Pointer",
      className: "_User",
      objectId: user.id
    })
    .find({useMasterKey: true, success: function(tmpSubscriptions){

      for (var key in tmpSubscriptions)
      {
        var subscription = tmpSubscriptions[key];

        subscriptions.push(subscription.get(Object.keys(fieldMap)[index]));
      }

      loop.next();
    },
    error: function(user, error)
    {
      error(_error);
      done.done = true;
    }
  });
  }, function(){

    success(subscriptions);
  });
}

AccountBalanceObserver.prototype.notify = function(subject, user)
{
  var _self = this;

  retrieveServicesAndPackagesSubscribed(user, function(subscriptions){
    _self.ruleObject.checkRule(new AccountBalanceProperties(user, subscriptions));
  },
  function(error)
  {

  });
}

exports.AccountBalanceObserver = AccountBalanceObserver;
