var BillPresenter = function(scope) {
  var _Self = this;
  this.angScope = scope;

  this.showUserBill = function(userSubscriptions){
    _Self.angScope.mySubs = userSubscriptions;
    _Self.angScope.$apply();
  }
}
