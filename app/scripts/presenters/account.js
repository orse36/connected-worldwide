var AccountPresenter = function(scope)
{
  var _Self = this
  this.angScope = scope;

  this.setMessage = function(id, message) {
      var elem = document.getElementById(id);

      if (elem){
      elem.className += " my-alert-active";
      elem.innerHTML = message;
      }
      console.log(message);
  }


  this.showInformations = function (infos){
      this.angScope.user = infos;
      this.angScope.$apply();
  }

  this.showErrorDifferentPasswords = function()
  {
    _Self.setMessage("statusMessage", "Passwords are different.");
  }

  this.showPasswordChanged = function()
  {
    _Self.setMessage("statusMessage", "Password changed.");
  }

  this.showErrorChangePassword = function(error)
  {
    _Self.setMessage("statusMessage", "Unable to change the password (" + error + ").");
  }

  this.showErrorPasswordTooWeak = function()
  {
    _Self.setMessage("statusMessage", "Password is too weak.");
  }

  this.showMaxAccountBalanceChanged = function()
  {
    _Self.setMessage("statusMessage", "Maximum account balance successfully changed.");
  }

  this.showErrorChangeMaxAccountBalance = function(error)
  {
    _Self.setMessage("statusMessage", "Unable to change the maximum account balance (" + error + ")");
  }
}
