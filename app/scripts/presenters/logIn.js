var LogInPresenter  = function(scope) {
    var _Self = this;
    this.angScope = scope;

    this.setMessage = function(id, message) {
        var elem = document.getElementById(id);

        if (elem){
        elem.className += " my-alert-active";
        elem.innerHTML = message;
        }
        console.log(message);
    }

    this.showSuccessfullyLogged = function(user)
    {
        window.location.replace("#/account");
    }

    this.showEmailNotVerified = function()
    {
        _Self.setMessage("statusMessage", "Email not verified yet.");
    }

    this.showErrorLogin = function()
    {
        _Self.setMessage("statusMessage", "Invalid username and/or password.");
    }

    this.showSuccessfullyRequestPassword = function()
    {
        _Self.setMessage("statusMessageRequestPassword", "Password reset, please check your inbox.");
    }

    this.showErrorRequestPassword = function(error)
    {
        _Self.setMessage("statusMessageRequestPassword", "There has been an error in your request, please try again.");
    }
}
