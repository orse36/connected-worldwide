'use strict';

var PackagePresenter = function(scope)
{
  var _Self = this;
  this.angScope = scope;

  this.setMessage = function(id, message) {
      var elem = document.getElementById(id);

      if (elem){
        elem.className += " my-alert-active";
        elem.innerHTML = message;
      }
      console.log(message);

  }

  this.showUserSubscriptions = function(userSubscriptions)
  {
    _Self.angScope.myPackages = userSubscriptions;
    _Self.angScope.$apply();
  }

  this.errorWithUserSubscriptions = function(error)
  {
    _Self.setMessage("statusMessage", "Error while retrieving user subscriptions: " + error);
  }

  this.showAvailablePackages = function(available)
  {
    _Self.angScope.av = available;
    _Self.angScope.$apply();
  }

  this.errorWithAvailablePackages = function(error)
  {
    _Self.setMessage("statusMessage", "Error while retrieving available packages: " + error);
  }

  this.showUserRoles = function(userRoles)
  {
    _Self.angScope.user = {};
    _Self.angScope.user.role = userRoles[0].get('name');
    _Self.angScope.$apply();
  }

  this.errorWithUserRoles = function()
  {
    _Self.setMessage("statusMessage", "Error while retrieving available user's role.");
  }

  this.showPackageCreated = function(_package)
  {
    _Self.angScope.avPackages.push(_package);
    _Self.angScope.$apply();
  }

  this.errorCreatePackage = function(error)
  {
    _Self.setMessage("statusMessage", "Error while creating a package: " + error.message);
  }

  this.showPackageSuccessfullyAdded = function()
  {
    _Self.setMessage("statusMessage", "Package successfully added.");
  }

  this.errorAddPackage = function(error)
  {
    _Self.setMessage("statusMessage", "Error while adding a package: " + error.message);
  }

  this.showPackageSuccessfullyCanceled = function()
  {
    _Self.setMessage("statusMessage", "Package successfully canceled.");
  }

  this.errorCancelPackage = function(error)
  {
    _Self.setMessage("statusMessage", "Error while canceling a package: " + error.message);
  }

  this.showPackageSuccessfullyDeleted = function()
  {
    _Self.setMessage("statusMessage", "Package successfully deleted.");
  }

  this.errorDeletePackage = function(error)
  {
    _Self.setMessage("statusMessage", "Error while deleting a package: " + error.message);
  }

  this.showPackagesCustomers = function(packagesCustomers)
  {
    var idx = 0;
    _Self.angScope.Customers = [];
    for (var i in packagesCustomers){
      _Self.angScope.Customers[i] = packagesCustomers[i].user.attributes;
      _Self.angScope.Customers[i].fullname = _Self.angScope.Customers[i].name + ' ' + _Self.angScope.Customers[i].firstname;
      _Self.angScope.Customers[i].packages = [];
      for (var key in packagesCustomers[i].subscriptions){
        _Self.angScope.Customers[i].packages[key] = packagesCustomers[i].subscriptions[key].get('package').attributes;
        _Self.angScope.Customers[i].packages[key].displayServices = packagesCustomers[i].subscriptions[key].get('package').relation("services");
      }
      if (_Self.angScope.Customers[i].username == _Self.username)
        idx = i;
    }
    _Self.angScope.selectedCustomer = _Self.angScope.Customers[idx];
    _Self.angScope.$apply();
  }

  this.errorPackagesCustomers = function(error)
  {
    _Self.setMessage("statusMessage", "Error while finding packages of customers: " + error.message);
  }

  this.showPackageAddedToCustomer = function(packageName, userName)
  {
    _Self.username = userName;
    /*for (var i in _Self.angScope.Customers){
      if (_Self.angScope.Customers[i].username == userName){
        _Self.angScope.dflt = i;
        break;
      }
    }*/
    //_Self.angScope.$apply();
    _Self.setMessage("statusMessage", "Package " + packageName + " has been added to " + userName);
  }

  this.errorAddPackageToCustomer = function(error)
  {
    _Self.setMessage("statusMessage", "Error while adding a package to a customer: " + error.message);
  }

  this.showPackageDeletedFromCustomer = function(packageName, userName)
  {
    _Self.username = userName;
    /*for (var i in _Self.angScope.Customers){
      if (_Self.angScope.Customers[i].username == userName){
        _Self.angScope.dflt= i;
        break;
      }
    }
    _Self.angScope.$apply();*/
    _Self.setMessage("statusMessage", "Package " + packageName + " has been deleted from " + userName);
  }

  this.errorDeletePackageToCustomer = function(error)
  {
    _Self.setMessage("statusMessage", "Error while deleting a package to a customer: " + error.message);
  }

  this.showModifiedPackage = function(packageName, serviceNames)
  {
    _Self.setMessage("statusMessage", "Successfully add " + serviceNames.join(", ") + " services to package: " + packageName);
  }

  this.errorModifyPackage = function(error)
  {
    _Self.setMessage("statusMessage", "Error while modifying services for package: " + error.message);
  }
}
