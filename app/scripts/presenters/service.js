'use strict';

var ServicePresenter = function(scope)
{
  var _Self = this;
  this.angScope = scope;

  this.setMessage = function(id, message) {
      var elem = document.getElementById(id);
      if (elem){
        elem.className += " my-alert-active";
        elem.innerHTML = message;
      }
      console.log(message);
  }

  this.showUserSubscriptions = function(userSubscriptions)
  {
    _Self.angScope.myServices = [];
    for (var key in userSubscriptions){
      _Self.angScope.myServices[key] = userSubscriptions[key].get('service').attributes;
    }
    _Self.angScope.$apply();
  }

  this.errorWithUserSubscriptions = function(error)
  {
    _Self.setMessage("statusMessage", "Error while retrieving user subscriptions: " + error);
  }

  this.showAvailableServices = function(availableServices)
  {
    _Self.angScope.avServices = [];
    for (var key in availableServices){
      _Self.angScope.avServices[key] = availableServices[key].attributes;
    }
    _Self.angScope.$apply();
  }

  this.errorWithAvailableServices = function(error)
  {
    _Self.setMessage("statusMessage", "Error while retrieving available services: " + error);
  }

  this.showUserRoles = function(userRoles)
  {
    _Self.angScope.user = {};
    _Self.angScope.user.role = userRoles[0].get('name');
    _Self.angScope.$apply();
  }

  this.errorWithUserRoles = function()
  {
    _Self.setMessage("statusMessage", "Error while retrieving available user's role.");
  }

  this.showServiceCreated = function(service)
  {
    _Self.angScope.avServices.push(service);
    _Self.angScope.$apply();
  }

  this.errorCreateService = function(error)
  {
    _Self.setMessage("statusMessage", "Error while creating a service: " + error.message);
  }

  this.showServiceSuccessfullyAdded = function()
  {
    _Self.setMessage("statusMessage", "Service successfully added.");
  }

  this.errorAddService = function(error)
  {
    _Self.setMessage("statusMessage", "Error while adding a service: " + error.message);
  }

  this.showServiceSuccessfullyCanceled = function()
  {
    _Self.setMessage("statusMessage", "Service successfully canceled.");
  }

  this.errorCancelService = function(error)
  {
    _Self.setMessage("statusMessage", "Error while canceling a service: " + error.message);
  }

  this.showServiceSuccessfullyDeleted = function()
  {
    _Self.setMessage("statusMessage", "Service successfully deleted.");
  }

  this.errorDeleteService = function(error)
  {
    console.log('TOTO');
    _Self.setMessage("statusMessage", "Error while deleting a service: " + error.message);
  }

  this.showServicesCustomers = function(servicesCustomers)
  {
    var idx = 0;
    _Self.angScope.Customers = [];
    for (var i in servicesCustomers){
      _Self.angScope.Customers[i] = servicesCustomers[i].user.attributes;
      _Self.angScope.Customers[i].fullname = _Self.angScope.Customers[i].name + ' ' + _Self.angScope.Customers[i].firstname;
      _Self.angScope.Customers[i].services = [];
      for (var key in servicesCustomers[i].subscriptions){
        _Self.angScope.Customers[i].services[key] = servicesCustomers[i].subscriptions[key].get('service').attributes;
      }
      if (_Self.angScope.Customers[i].username == _Self.username)
        idx = i;
    }
    _Self.angScope.selectedCustomer = _Self.angScope.Customers[idx];
    _Self.angScope.$apply();
  }

  this.errorServicesCustomers = function(error)
  {
    _Self.setMessage("statusMessage", "Error while finding services of customers: " + error.message);
  }

  this.showServiceAddedToCustomer = function(serviceName, userName)
  {
    _Self.username = userName;
    /*for (var i in _Self.angScope.Customers){
      if (_Self.angScope.Customers[i].username == userName){
        _Self.angScope.dflt = i;
        break;
      }
    }*/
    //_Self.angScope.$apply();
    _Self.setMessage("statusMessage", "Service " + serviceName + " has been added to " + userName);
  }

  this.errorAddServiceToCustomer = function(error)
  {
    _Self.setMessage("statusMessage", "Error while adding a service to a customer: " + error.message);
  }

  this.showServiceDeletedFromCustomer = function(serviceName, userName)
  {
    _Self.username = userName;
    /*for (var i in _Self.angScope.Customers){
      if (_Self.angScope.Customers[i].username == userName){
        _Self.angScope.dflt= i;
        break;
      }
    }
    _Self.angScope.$apply();*/
    _Self.setMessage("statusMessage", "Service " + serviceName + " has been deleted from " + userName);
  }

  this.errorDeleteServiceToCustomer = function(error)
  {
    _Self.setMessage("statusMessage", "Error while deleting a service to a customer: " + error.message);
  }
}
