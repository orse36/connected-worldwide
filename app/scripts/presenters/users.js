var UsersPresenter = function(scope) {
  var _Self = this;
  this.angScope = scope;

  this.setMessage = function(id, message) {
      var elem = document.getElementById(id);

      if (elem){
      elem.className += " my-alert-active";
      elem.innerHTML = message;
      }
      console.log(message);
  }

  this.showSuccess = function(role)
  {
    _Self.setMessage("statusMessage", role + " created");
  }

  this.showError = function(error)
  {
    _Self.setMessage("statusMessage", "Error: " + error.message);
  }

  this.showUserRoles = function(userRoles)
  {
    _Self.angScope.user = {};
    _Self.angScope.user.role = userRoles[0].get('name');
    _Self.angScope.$apply();
  }
}
