var SignUpPresenter = function(scope) {
  var _Self = this;
  this.angScope = scope;

  this.setMessage = function(id, message) {
      var elem = document.getElementById(id);

      if (elem){
      elem.className += " my-alert-active";
      elem.innerHTML = message;
      }
      console.log(message);
  }


  this.showSuccessfullySignedUp = function()
  {
    window.location.replace("#/account");
  }

  this.showErrorSignUp = function(error)
  {
    _Self.setMessage("statusMessage", "Error: " + error.message);
  }

  this.showErrorPasswordTooWeak = function()
  {
    _Self.setMessage("statusMessage", "Your password is to weak.");
  }
}
