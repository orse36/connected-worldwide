'use strict';

// Declaring class with static methods.
function PackageDataStore() {};

PackageDataStore.retrieveSubscribedPackages = function(success, error)
{
  Parse.Cloud.run("getSubscribedPackages", {user: Parse.User.current().id}, {success: success, error: error});
}

PackageDataStore.retrieveAvailablePackages = function(success, error)
{
  Parse.Cloud.run("getPackages", {}, {success: success, error: error});
}

PackageDataStore.addPackage = function(packageName, success, error)
{
  var userId = Parse.User.current().id;

  Parse.Cloud.run("addPackage", { userId: userId, packageName: packageName}, {success: success, error: error});
}

PackageDataStore.cancelPackage = function(packageName, success, error)
{
  var userId = Parse.User.current().id;

  Parse.Cloud.run("cancelPackage", { userId: userId, packageName: packageName}, {success: success, error: error});
}

PackageDataStore.createPackage = function(_package, success, error)
{
  var userId = Parse.User.current().id;

  Parse.Cloud.run("createPackage", { userId: userId, test: _package, _package: _package}, {success: success, error: error});
}

PackageDataStore.deletePackage = function(packageName, success, error)
{
  var userId = Parse.User.current().id;

  Parse.Cloud.run("deletePackage", { userId: userId, packageName: packageName}, {success: success, error: error});
}

PackageDataStore.retrievePackagesCustomers = function(success, error)
{
  var userId = Parse.User.current().id;

  Parse.Cloud.run("retrieveCustomers", {userId: userId, field: "package"}, {success: success, error: error});
}

PackageDataStore.addPackageToCustomer = function(packageName, userName, success, error)
{
  var userId = Parse.User.current().id;
  Parse.Cloud.run("addPackageToCustomer", {currentUserId: userId, userName: userName, packageName: packageName}, {success: success, error: error});
}

PackageDataStore.deletePackageToCustomer = function(packageName, userName, success, error)
{
  var userId = Parse.User.current().id;

  Parse.Cloud.run("deletePackageToCustomer", {currentUserId: userId, userName: userName, packageName: packageName}, {success: success, error: error});
}

PackageDataStore.modifyPackageServices = function(packageName, serviceNames, success, error)
{
  var userId = Parse.User.current().id;

  Parse.Cloud.run("modifyPackageServices", {currentUserId: userId, packageName: packageName, serviceNames: serviceNames}, {success: success, error: error});
}
