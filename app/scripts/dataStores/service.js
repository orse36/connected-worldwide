'use strict';

// Declaring class with static methods.
function ServiceDataStore() {};

ServiceDataStore.retrieveSubscribedServices = function(success, error)
{
  var Subscription = Parse.Object.extend("Subscription");
  var query = new Parse.Query(Subscription);

  query
  .equalTo("user", Parse.User.current())
  .include("service")
  .find(success, error);
}

ServiceDataStore.retrieveAvailableServices = function(success, error)
{
  var Service = Parse.Object.extend("Service");
  var query = new Parse.Query(Service);

  query.find(success, error);
}

ServiceDataStore.addService = function(serviceName, success, error)
{
  var userId = Parse.User.current().id;

  Parse.Cloud.run("addService", { userId: userId, serviceName: serviceName}, {success: success, error: error});
}

ServiceDataStore.cancelService = function(serviceName, success, error)
{
  var userId = Parse.User.current().id;

  Parse.Cloud.run("cancelService", { userId: userId, serviceName: serviceName}, {success: success, error: error});
}

ServiceDataStore.createService = function(service, success, error)
{
  var userId = Parse.User.current().id;

  Parse.Cloud.run("createService", { userId: userId, test: service, service: service}, {success: success, error: error});
}

ServiceDataStore.deleteService = function(serviceName, success, error)
{
  var userId = Parse.User.current().id;

  Parse.Cloud.run("deleteService", { userId: userId, serviceName: serviceName}, {success: success, error: error});
}

ServiceDataStore.retrieveServicesCustomers = function(success, error)
{
  var userId = Parse.User.current().id;

  Parse.Cloud.run("retrieveCustomers", {userId: userId, field: "service"}, {success: success, error: error});
}

ServiceDataStore.addServiceToCustomer = function(serviceName, userName, success, error)
{
  var userId = Parse.User.current().id;
  Parse.Cloud.run("addServiceToCustomer", {currentUserId: userId, userName: userName, serviceName: serviceName}, {success: success, error: error});
}

ServiceDataStore.deleteServiceToCustomer = function(serviceName, userName, success, error)
{
  var userId = Parse.User.current().id;

  Parse.Cloud.run("deleteServiceToCustomer", {currentUserId: userId, userName: userName, serviceName: serviceName}, {success: success, error: error});
}
