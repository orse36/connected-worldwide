// 'use strict';
//
// // Declaring class with static methods.
// function LoginRecordDataStore() {};
//
// LoginRecordDataStore.createLoginRecord = function(email){
//   var loginRecord = new Parse.Object("LoginRecord");
//   loginRecord.set("email", email);
//   loginRecord.set("numberFail", 0);
//   loginRecord.save();
// }
//
// LoginRecordDataStore.resetLoginRecord = function(email){
//   var lR = Parse.Object.extend("LoginRecord");
//   var query = new Parse.Query(lR);
//
//   query
//   .equalTo("email", email)
//   .limit(1)
//   .find({
//     success: function (results){
//       if (results.length > 0){
//         var res = results[0];
//         res.set("numberFail", 0);
//         res.save();
//       }
//     },
//     error: function(error){
//
//     }
//   });
// }
//
// LoginRecordDataStore.checkLoginRecord = function(email){
//   var lR = Parse.Object.extend("LoginRecord");
//   var query = new Parse.Query(lR);
//
//   query
//   .equalTo("email", email)
//   .limit(1)
//   .find({
//     success: function (results){
//       if (results.length > 0){
//         var res = results[0];
//         res.increment("numberFail");
//         res.save();
//         if (res.attributes.numberFail % 3 == 0){
//           Parse.Cloud.run("sendMail", {emailTo: email, subject: "[Warning] Connected Worldwide", text: "We have register at least 3 failed attempt to log on your account. We strongly recommand you to change your password!"}, {
//             success: function(msg) {
//               console.log(msg);
//             },
//             error: function(error) {
//               console.log(error);
//             }
//           });
//         }
//       }
//     },
//     error: function(error){
//       console.log("error: " + error);
//     }
//   });
// }
