'use strict';

// Declaring class with static methods.
function UserDataStore() {}

UserDataStore.createUser = function(infos, success, error){
    Parse.Cloud.run("signUp", infos,
    {
        success: success,
        error: error
    });
};

UserDataStore.createCR = function(infos, success, error){
    Parse.Cloud.run("createCR", infos,
    {
        success: success,
        error: error
    });
};

UserDataStore.createMR = function(infos, success, error){
    Parse.Cloud.run("createMR", infos,
    {
        success: success,
        error: error
    });
};

UserDataStore.logIn = function(infos, success, error){
    Parse.Cloud.run("logIn", {email: infos.email,
        password: infos.password,
    },
    {
        success: function(userSessionToken)
        {
            Parse.User.become(userSessionToken).then(function (user) {
                success(user);
            }, function (error) {
                console.log("error while login: " + error);
                error(error);
            });
        },
        error: error
    });
};

UserDataStore.logOut = function(){
    Parse.User.logOut();
};

UserDataStore.userLogged = function(){
    return Parse.User.current();
};

UserDataStore.changePassword = function(password, success, error){
    var user = Parse.User.current();

    if (user){
        user.set("password", password);
        user.save()
        .then(
            function(user){ success(); },
            function(error){ error(error); }
        );
    }
    else{
        error(null);
    }
}

UserDataStore.requestPassword = function(email, success, error)
{
    Parse.User.requestPasswordReset(email, {
        success: function() {
            success()
        },
        error: function(err) {
            error(err)
        }
    });
}

UserDataStore.retrieveUserRoles = function(success, error)
{
    if (Parse.User.current() != null) {
        var userId = Parse.User.current().id;

        Parse.Cloud.run("getUserRoles", { userId: userId }, {
            success: success,
            error: error
        });
    }
}

UserDataStore.changeMaxAccountBalance = function(max, success, error)
{
  if (Parse.User.current() != null)
  {
    var userId = Parse.User.current().id;

    Parse.Cloud.run("changeMaxAccountBalance", { userId: userId, max: max }, {
      success: function(){

        Parse.User.current().fetch().then(function(user){
          success();
        }, function(_error){
          error(_error);
        })
      },
      error: error
    });
  }
}

UserDataStore.getFees = function(success, error){
  Parse.Cloud.run("getFees", {userId: Parse.User.current().id}, {success: success, error: error});
}
