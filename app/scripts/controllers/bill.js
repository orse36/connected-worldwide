/**
 * Created by jctwilliam on 2/1/15.
 */

angular.module('connectedWorldwideApp')
  .controller('BillCtrl', function ($scope, $location) {
    if (Parse.User.current() == null)
      $location.path('/login');
    $scope.ctrl = new BillCtrl($scope);
    $scope.ctrl.findSubscribedServices();
  });
