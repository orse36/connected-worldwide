'use strict';

/**
 * @ngdoc function
 * @name connectedWorldwideApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the connectedWorldwideApp
 */

angular.module('connectedWorldwideApp')
  .controller('LogInCtrl', function ($scope) {
    $scope.ctrl = new LogInCtrl();
  });
