'use strict';
/**
 * @ngdoc function
 * @name connectedWorldwideApp.controller:SignUpCtrl
 * @description
 * # SignUpCtrl
 * Controller of the connectedWorldwideApp
 */

 angular.module('connectedWorldwideApp')
  .controller('SignUpCtrl', function ($scope) {
    $scope.ctrl = new SignUpCtrl();
  });
