'use strict';

angular.module('connectedWorldwideApp')
  .controller('UsersCtrl', function ($scope, $location) {
    if (Parse.User.current() == null)
      $location.path('/login');

    $scope.ctrl = new UsersCtrl($scope);
    $scope.ctrl.findUserRoles();
  });
