'use strict';

angular.module('connectedWorldwideApp')
  .controller('PackageCtrl', function ($scope, $location) {
    if (Parse.User.current() == null)
      $location.path('/login');
    $scope.ctrl = new PackageCtrl($scope);
    $scope.ctrl.findUserRoles();
    $scope.ctrl.findSubscribedPackages();
    $scope.ctrl.findAvailablePackages();

    $scope.addPkgSrv = {};
    $scope.modifyPkgSrv = {};
  });
