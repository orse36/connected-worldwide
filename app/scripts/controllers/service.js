'use strict';

angular.module('connectedWorldwideApp')
  .controller('ServiceCtrl', function ($scope, $location) {
    if (Parse.User.current() == null)
      $location.path('/login');
    $scope.ctrl = new ServiceCtrl($scope);
    $scope.ctrl.findUserRoles();
    $scope.ctrl.findSubscribedServices();
    $scope.ctrl.findAvailableServices();

    // $scope.ctrl.deleteServiceToCustomer("test 2", "orangel@ucsd.edu");

    //$scope.ctrl.findServicesCustomers();

    // $scope.ctrl.addService("test 2");
    // $scope.ctrl.findSubscribedServices();

    // $scope.ctrl.findUserRoles();
    // $scope.ctrl.addService("test 2");
    // setTimeout(function () {
    //   $scope.ctrl.findSubscribedServices();
    //   }, 3000);
    // $scope.ctrl.addService("test 2");
    // setTimeout(function () {
    //   $scope.ctrl.cancelService("test 2");
    //   setTimeout(function () {
    //     $scope.ctrl.findSubscribedServices();
    //     }, 3000);
    //   }, 3000);
    // $scope.ctrl.findAvailableServices();
    // $scope.ctrl.deleteService("test name");
    // setTimeout(function () {
    //   $scope.ctrl.createService("test name", 30, "test description");
    //   }, 3000);


    // var Services = Parse.Object.extend("Services");
    // var query = new Parse.Query(Services);
    // query.find({
    //   success: function(results) {
    //     var asl = [];
    //     for (var i = 0; i < results.length; i++) {
    //       var object = results[i];
    //       asl.push(object.get('name'));
    //     }
    //     document.getElementById("services").innerHTML = asl;
    //   },
    //   error: function(error) {
    //     alert("Error: " + error.code + " " + error.message);
    //   }
    // });

    // var Subscription = Parse.Object.extend("Subscription");
    // query = new Parse.Query(Subscription);
    //
    // query.equalTo("user", $scope.user.email);
    // query.find({
    //   success: function(results) {
    //     var msl = [];
    //     for (var i = 0; i < results.length; i++) {
    //       var object = results[i];
    //       msl.push(object.get('service'));
    //     }
    //     document.getElementById("myServices").innerHTML = msl;
    //   },
    //   error: function(error) {
    //     alert("Error: " + error.code + " " + error.message);
    //   }
    // });
  });
