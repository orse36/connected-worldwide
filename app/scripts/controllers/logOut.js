'use strict';

angular.module('connectedWorldwideApp')
.controller('LogOutCtrl', function ($scope) {
  if (Parse.User.current() == null)
    $location.path('/login');
  var logOutCtrl = new LogOutCtrl();

  logOutCtrl.logOut()
});
