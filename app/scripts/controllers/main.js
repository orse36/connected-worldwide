'use strict';

/**
 * @ngdoc function
 * @name connectedWorldwideApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the connectedWorldwideApp
 */

angular.module('connectedWorldwideApp')
  .controller('MainCtrl', function ($scope, $location) {
    $scope.isActive = function(route) {
        return route === $location.path();
    }

    $scope.ctrl = new MainCtrl();
    $scope.ctrl.findUserRoles();
  });
