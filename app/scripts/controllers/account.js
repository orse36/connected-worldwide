'use strict';

angular.module('connectedWorldwideApp')
.controller('AccountCtrl', function ($scope, $location) {
  if (Parse.User.current() == null)
    $location.path('/login');
  $scope.ctlr = new AccountCtrl($scope);
  $scope.ctlr.getInfos();
  console.log('Test');
});
