'use strict';

function getCustomerType(user) {
    if (typeof user !== 'undefined') {
        if (typeof user.accountType !== 'undefined') {
            return user.accountType;
        }
    }

    return undefined;
}

var UsersCtrl = function(scope)
{
    this.presenter = new UsersPresenter(scope);
    var _Self = this;

    this.createCustomer = function(infos) {
        var success = function (user) {
            UserDataStore.requestPassword(infos.email, function(){
                _Self.presenter.showSuccess("Customer");
            }, function(error){
                _Self.presenter.showError(error);
            });
        }

        var error = function (err){
            _Self.presenter.showError(err);
        }


        infos.username = infos.email;
        infos.password = Math.random().toString(36).substr(2);
        if (infos.accountType != 'retail') {
            infos.firstName = '';
        }

        UserDataStore.createUser(infos, success, error);
    }

    this.createCustomerRep = function(infos) {
        var success = function (user) {
            UserDataStore.requestPassword(infos.email, function(){
                _Self.presenter.showSuccess("Customer Representative");
            }, function(error){
                _Self.presenter.showError(error);
            });
        }

        var error = function (err){
            _Self.presenter.showError(err);
        }

        infos.password = Math.random().toString(36).substr(2);

        UserDataStore.createCR(infos, success, error);
    }

    this.createMarketingRep = function(infos) {
        var success = function (user) {
            UserDataStore.requestPassword(infos.email, function(){
                _Self.presenter.showSuccess("Marketing Representative");
            }, function(error){
                _Self.presenter.showError(error);
            });
        }

        var error = function (err){
            _Self.presenter.showError(err);
        }

        infos.password = Math.random().toString(36).substr(2);

        UserDataStore.createMR(infos, success, error);
    }

    this.isRetail = function(user) {
        return getCustomerType(user) == 'retail';
    }

    this.isCommercial = function(user) {
        return getCustomerType(user) == 'commercial';
    }

    this.hasAType = function(user) {
        return getCustomerType(user) != undefined;
    }

    this.findUserRoles = function()
    {
        UserDataStore.retrieveUserRoles(function(userRoles){
            _Self.presenter.showUserRoles(userRoles);
        }, function(){
            _Self.presenter.showError({message: "Error while retrieving user roles"});
        });
    }
};
