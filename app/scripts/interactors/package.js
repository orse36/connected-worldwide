'use strict';

var PackageCtrl = function(scope)
{
  this.presenter = new PackagePresenter(scope);
  var _Self = this;


  this.findServices = function(pkg, avServ){
    if (!pkg.services.query)
      return ;
    pkg.displayServices.query().find({
      success: function(services){
        pkg.services = [];
        for(var k in services){
          pkg.services.push(services[k].attributes);
        }
        _Self.presenter.angScope.$apply();
      },
      error: function(error){
        console.log('Error: ' + error);
      }
    });
  }

  this.setServicesCheck = function(pkg){
    for (var k in pkg.services){
        _Self.presenter.angScope.modifyPkgSrv[pkg.services[k].name] = true;
    }
  }

  this.isServiceInPackage = function (service, pkg){
    for (var k in pkg.services){
      if (service == pkg.services[k].name){
        return true;
      }
    }
    return false;
  }

  this.findSubscribedPackages = function()
  {
    PackageDataStore.retrieveSubscribedPackages(function(userSubscriptions){
      _Self.presenter.showUserSubscriptions(userSubscriptions);
    },
    function(error){
      _Self.presenter.errorWithUserSubscriptions(error);
    });
  }

  this._retrieveAvailablePackages = function(success, error)
  {
    var av = {
      services: [],
      packages: []
    };
    PackageDataStore.retrieveAvailablePackages(function(availablePackages){
      av.packages = availablePackages;
      ServiceDataStore.retrieveAvailableServices(function(avServ){
        for (var k in avServ){
          av.services.push(avServ[k].attributes);
        }
        success(av);
      }, function(err){
        error(err);
      });
    },
    function(err){
      error(err);
    });
  }

  this.findAvailablePackages = function()
  {
    _Self._retrieveAvailablePackages(function(availablePackages){
      _Self.presenter.showAvailablePackages(availablePackages);
    },
    function(error){
      _Self.presenter.errorWithAvailablePackages(error);
    });
  }

  this.findUserRoles = function()
  {
    UserDataStore.retrieveUserRoles(function(userRoles){
      if (userRoles[0].get('name') != "RetailCustomer"
        && userRoles[0].get('name') != "CommercialCustomer"){
          _Self.findPackagesCustomers();
      }
      _Self.presenter.showUserRoles(userRoles);
    }, function(){
      _Self.presenter.errorWithUserRoles();
    });
  }

  this.createPackage = function(_package)
  {
    PackageDataStore.createPackage(_package, function(){
      _Self.presenter.setMessage("statusMessage", "Package succesfully created!");
      _Self.findAvailablePackages();
    }, function(error){
      _Self.presenter.errorCreatePackage(error);
    });
  }

  //This function is for a customer to add a package
  this.addPackage = function(packageName){
    PackageDataStore.addPackage(packageName, function(){
      _Self.presenter.showPackageSuccessfullyAdded();
      _Self.findSubscribedPackages();
    },
    function(error){
      _Self.presenter.errorAddPackage(error);
    })
  };

  this.cancelPackage = function(packageName)
  {
    PackageDataStore.cancelPackage(packageName, function(){
      _Self.presenter.showPackageSuccessfullyCanceled();
      _Self.findSubscribedPackages();
    },
    function(error){
      _Self.presenter.errorCancelPackage(error);
    })
  }

  this.deletePackage = function(packageName){
    PackageDataStore.deletePackage(packageName, function(){
      _Self.presenter.showPackageSuccessfullyDeleted();
      _Self.findAvailablePackages();
    },
    function(error){
      _Self.presenter.errorDeletePackage(error);
      _Self.findAvailablePackages();
    })
  }

  this.findPackagesCustomers = function(){
    PackageDataStore.retrievePackagesCustomers(function(packagesCustomers){
      _Self.presenter.showPackagesCustomers(packagesCustomers)
    },
    function(error){
      _Self.presenter.errorPackagesCustomers(error);
    })
  }

  this.addPackageToCustomer = function(packageName, userName){
    PackageDataStore.addPackageToCustomer(packageName, userName, function(){
      _Self.presenter.showPackageAddedToCustomer(packageName, userName);
      _Self.findPackagesCustomers();
    },
    function(error){
      _Self.presenter.errorAddPackageToCustomer(error);
    })
  }

  this.deletePackageToCustomer = function(packageName, userName){
    PackageDataStore.deletePackageToCustomer(packageName, userName, function(){
      _Self.presenter.showPackageDeletedFromCustomer(packageName, userName);
      _Self.findPackagesCustomers();
    },
    function(error){
      _Self.presenter.errorDeletePackageToCustomer(error);
    })
  }

  this.modifyPackage = function(pkgName, services){
    var strServNames = [];
    for (var k in services){
      if (services[k] == true)
        strServNames.push(k);
    }
    PackageDataStore.modifyPackageServices(pkgName.name, strServNames, function(){
      _Self.findAvailablePackages();
    },
    function(error){
      _Self.presenter.errorModifyPackage(error);
    })
  }
};
