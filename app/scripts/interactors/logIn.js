var LogInCtrl  = function(scope) {
  this.presenter = new LogInPresenter(scope);
  var _Self = this;

  this.logIn = function(infos) {
    var success = function (user) {
      if (user.attributes.emailVerified) {
        _Self.presenter.showSuccessfullyLogged(user);
      }
      else {
        UserDataStore.logOut();
        _Self.presenter.showEmailNotVerified();
     }
   };

    var error = function (user, err){
      _Self.presenter.showErrorLogin();
    };

    UserDataStore.logIn(infos, success, error);
  }

  this.requestPassword = function(email)
  {
    UserDataStore.requestPassword(email, function(){
      _Self.presenter.showSuccessfullyRequestPassword();
    }, function(error){
      _Self.presenter.showErrorRequestPassword();
    })
  }
}
