
var SignUpCtrl = function(scope) {
  this.presenter = new SignUpPresenter(scope);
  var _Self = this;

  this.getCustomerType = function(user) {
    if (typeof user !== 'undefined') {
      if (typeof user.accountType !== 'undefined') {
        return user.accountType;
      }
    }
    return undefined;
  }

  this.signUp = function(infos) {
    var success = function (user) {
      _Self.presenter.showSuccessfullySignedUp();
    }

    var error = function (err) {
      _Self.presenter.showErrorSignUp(err);
    }

    var passwordScore = CheckForm.getPasswordScore(infos.password);

    if (passwordScore < 1)
    {
      _Self.presenter.showErrorPasswordTooWeak()
    }
    else
    {
      infos.username = infos.email;
      if (infos.accountType != 'retail') {
          infos.firstName = '';
          infos.birthday = '1970-01-01';
      }

      UserDataStore.createUser(infos, success, error);
    }
  }

  this.isRetail = function(user) {
      return this.getCustomerType(user) == 'retail';
  }

  this.isCommercial = function(user) {
      return this.getCustomerType(user) == 'commercial';
  }

  this.hasAType = function(user) {
      return this.getCustomerType(user) != undefined;
  }
}
