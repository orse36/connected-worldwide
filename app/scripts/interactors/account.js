var AccountCtrl = function(scope)
{
  this.presenter = new AccountPresenter(scope);
  var _Self = this;

  this.getInfos = function(){
      infos = Parse.User.current().attributes;
      UserDataStore.retrieveUserRoles(function (role){
        infos.userType = role[0].attributes.displayName;
        infos.role = role[0].attributes.name;
        _Self.presenter.showInformations(infos);
        },
      function(err){console.log('Error while retrieving userRole: ' + err)});
  }

  this.changePassword = function(infos)
  {
    if (infos.password !== infos.checkPassword)
    {
      _Self.presenter.showErrorDifferentPasswords();
    }
    else if (CheckForm.getPasswordScore(infos.password) < 40)
    {
      _Self.presenter.showErrorPasswordTooWeak();
    }
    else
    {
      UserDataStore.changePassword(infos.password, function(user){
        _Self.presenter.showPasswordChanged();
      }, function(error){
        _Self.presenter.showErrorChangePassword(error);
      })
    }
  }

  this.changeMaxAccountBalance = function(max)
  {
    UserDataStore.changeMaxAccountBalance(max, function(){
      _Self.presenter.showMaxAccountBalanceChanged();
    }, function(error){
      _Self.presenter.showErrorChangeMaxAccountBalance(error.message);
    });
  }
}
