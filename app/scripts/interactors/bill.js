'use strict';

var BillCtrl = function(scope)
{
  var _Self = this;
  this.presenter = new BillPresenter(scope);

  function getBill(){
    var userSubscriptions = {
      services: [],
      packages: [],
      fees: 0,
      total: 0
    };
    UserDataStore.getFees(function(fees){
      userSubscriptions.fees = fees;
      userSubscriptions.total += fees;
      ServiceDataStore.retrieveSubscribedServices(function(userServices){
        for (var key in userServices){
          userSubscriptions.total += userServices[key].get('service').attributes.price;
          userSubscriptions.services.push(userServices[key].get('service').attributes);
        }
        PackageDataStore.retrieveSubscribedPackages(function(userPackages){
          for (var key in userPackages){
            userSubscriptions.total += userPackages[key].price;
            userSubscriptions.packages.push(userPackages[key]);
          }
          _Self.presenter.showUserBill(userSubscriptions);
        },
        function (error) {
          _Self.presenter.angScope.errors = error;
        });
      },
      function(error){
        _Self.presenter.angScope.errors = error;
      });
    },
    function(error){
      _Self.presenter.angScope.errors = error;
    });
  }

  function getAllBill(){
    var users = [];
    var userSubscriptions = {
      services: [],
      packages: [],
      fees: 0,
      total: 0
    };
  }

  this.findSubscribedServices = function(){
    UserDataStore.retrieveUserRoles(function(roles){
      var role = roles[0].get('displayName');
      _Self.presenter.angScope.userRole = role;
      if (role == ('Customer' || 'Company representative')){
        getBill();
      }
      else{
        getAllBill();
      }
    }, function(err){
      _Self.presenter.angScope.errors = "Invalid User";
    })

  };
};
