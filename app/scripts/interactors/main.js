var MainCtrl = function()
{
  var _Self = this;

  this.userLogged = function()
  {
    return UserDataStore.userLogged();
  }

  this.findUserRoles = function()
  {
    UserDataStore.retrieveUserRoles(function(userRoles){
      _Self.userRole = userRoles[0].get('name');
    }, function(){
    });
  }

  this.isCustomer = function()
  {
    return(Parse.User.current()
      && (_Self.userRole == 'RetailCustomer' || _Self.userRole == 'CommercialCustomer'));
  }
}
