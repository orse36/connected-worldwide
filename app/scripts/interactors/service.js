'use strict';

var ServiceCtrl = function(scope)
{
  this.presenter = new ServicePresenter(scope);
  var _Self = this;

  this.findSubscribedServices = function()
  {
    ServiceDataStore.retrieveSubscribedServices(function(userSubscriptions){
      _Self.presenter.showUserSubscriptions(userSubscriptions);
    },
    function(error){
      _Self.presenter.errorWithUserSubscriptions(error);
    });
  }

  this._retrieveAvailableServices = function(success, error)
  {
    ServiceDataStore.retrieveAvailableServices(function(availableServices){
      success(availableServices);
    },
    function(error){
      error(error);
    });
  }

  this.findAvailableServices = function()
  {
    _Self._retrieveAvailableServices(function(availableServices){
      _Self.presenter.showAvailableServices(availableServices);
    },
    function(error){
      _Self.presenter.errorWithAvailableServices(error);
    });
  }

  this.findUserRoles = function()
  {
    UserDataStore.retrieveUserRoles(function(userRoles){
      if (userRoles[0].get('name') != "RetailCustomer"
        && userRoles[0].get('name') != "CommercialCustomer"){
          _Self.findServicesCustomers();
      }
      _Self.presenter.showUserRoles(userRoles);
    }, function(){
      _Self.presenter.errorWithUserRoles();
    });
  }

  this.createService = function(service)
  {
    ServiceDataStore.createService(service, function(){
      _Self.presenter.setMessage("statusMessage", "Service succesfully created!");
      _Self.findAvailableServices();
    }, function(error){
      _Self.presenter.errorCreateService(error);
    });
  }

  //This function is for a customer to add a service
  this.addService = function(serviceName){
    ServiceDataStore.addService(serviceName, function(){
      _Self.presenter.showServiceSuccessfullyAdded();
      _Self.findSubscribedServices();
    },
    function(error){
      _Self.presenter.errorAddService(error);
    })
  };

  this.cancelService = function(serviceName)
  {
    ServiceDataStore.cancelService(serviceName, function(){
      _Self.presenter.showServiceSuccessfullyCanceled();
      _Self.findSubscribedServices();
    },
    function(error){
      _Self.presenter.errorCancelService(error);
    })
  }

  this.deleteService = function(serviceName){
    ServiceDataStore.deleteService(serviceName, function(){
      _Self.presenter.showServiceSuccessfullyDeleted();
      _Self.findAvailableServices();
    },
    function(error){
      _Self.presenter.errorDeleteService(error);
      _Self.findAvailableServices();
    })
  }

  this.findServicesCustomers = function(){
    ServiceDataStore.retrieveServicesCustomers(function(servicesCustomers){
      _Self.presenter.showServicesCustomers(servicesCustomers)
    },
    function(error){
      _Self.presenter.errorServicesCustomers(error);
    })
  }

  this.addServiceToCustomer = function(serviceName, userName){
    ServiceDataStore.addServiceToCustomer(serviceName, userName, function(){
      _Self.presenter.showServiceAddedToCustomer(serviceName, userName);
      _Self.findServicesCustomers();
    },
    function(error){
      _Self.presenter.errorAddServiceToCustomer(error);
    })
  }

  this.deleteServiceToCustomer = function(serviceName, userName){
    ServiceDataStore.deleteServiceToCustomer(serviceName, userName, function(){
      _Self.presenter.showServiceDeletedFromCustomer(serviceName, userName);
      _Self.findServicesCustomers();
    },
    function(error){
      _Self.presenter.errorDeleteServiceToCustomer(error);
    })
  }
};
