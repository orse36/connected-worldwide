'use strict';

/**
 * @ngdoc overview
 * @name connectedWorldwideApp
 * @description
 * # connectedWorldwideApp
 *
 * Main module of the application.
 */
angular
  .module('connectedWorldwideApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/signup', {
        templateUrl: 'views/signup.html',
        controller: 'SignUpCtrl'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LogInCtrl'
      })
      .when('/logout', {
        templateUrl: 'views/main.html',
        controller: 'LogOutCtrl'
      })
      .when('/account', {
        templateUrl: 'views/account.html',
        controller: 'AccountCtrl'
      })
      .when('/service', {
        templateUrl: "views/service.html",
        controller: 'ServiceCtrl'
      })
      .when('/package', {
        templateUrl: "views/package.html",
        controller: 'PackageCtrl'
      })
      .when('/bill', {
        templateUrl: 'views/bill.html',
        controller: 'BillCtrl'
      })
      .when('/users', {
        templateUrl: 'views/users.html',
        controller: 'UsersCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .run(function($rootScope, $location) {
    Parse.initialize('yGFjG43FXdtKlMEMK9CCQc7lJjua6wsWvgvZLnIM', 'oYXQFRKhrsY6RteEF7kVKOL6VUKraO7Xdl1bMrzv');
  })
  .directive("passwordStrength", function(){
    return {
      restrict: 'A',
      link: function(scope, element, attrs){
        scope.$watch(attrs.passwordStrength, function(value) {
          if(angular.isDefined(value)){
            var value = CheckForm.getPasswordScore(value)

            if (value > 80)
            {
              scope.strength = 'strong';
            }
            else if (value >= 40)
            {
              scope.strength = 'medium';
            }
            else
            {
              scope.strength = 'weak';
            }
          }
        });
      }
    };
  });
