# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo is for the project of the group "Connected Worldwide" for the class "CSE 110 Software Engineering" at UCSD.

### How do I get set up? ###

In order to runthis project, you need to have an UNIX system.

Set up steps:
- install nodejs (http://nodejs.org/).
- at the root of the project, run "npm install".
- Then run "grunt serve" and enjoy!

If any error message appear, follow the guidelines and install any missing module with "npm". If really your are blocked, open an issue with your error message.

### Contribution guidelines ###

* Every new feature should be provided with a set of unit tests.
* When you develop a new feature or a fix, do it in a new branche name : "fix_NAME" or "feature_NAME". Then make a pull request when your feature is completed.
