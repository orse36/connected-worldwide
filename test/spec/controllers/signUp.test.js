'use strict';

describe('Controller: SignUpCtrl', function () {

  // load the controller's module
  beforeEach(module('connectedWorldwideApp'));

  var SignUpCtrl,
    user,
    scope;

  describe('Delete self customer user', function () {
    beforeEach(function (done) {
      UserDataStore.logIn({email: 'c2ji@ucsd.edu', password: 'test'},
        function (newUser) {
          user = newUser;
          inject(function ($controller, $rootScope) {
            scope = $rootScope.$new();
            SignUpCtrl = $controller('SignUpCtrl', {
              $scope: scope
            });
          });
          done()
        },
        function (error) {
          done()
        });
    });

    it("Test the interactor", function () {
      var test = {
        "username": "test",
        "password": "test",
        "email": "test@cw.com",
        "birthday": "2015-01-01",
        "address": "test's home",
        "state": "test's state",
        "zip": "111111",
        "city": "test's city",
        "firstname": "test",
        "lastname": "test"
      };
      scope.ctrl.signUp(test);
      scope.ctrl.isRetail(Parse.User.current());
      scope.ctrl.isCommercial(Parse.User.current());
      scope.ctrl.hasAType(Parse.User.current());
      getCustomerType(Parse.User.current());
    });
  });
});
