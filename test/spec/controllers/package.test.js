'use strict';

describe('Controller: PackageCtrl', function () {

  // load the controller's module
  beforeEach(module('connectedWorldwideApp'));

  var PackageCtrl,
    user,
    scope;

  describe('Package Functionality Test', function(){
    beforeEach(function(done){
      UserDataStore.logIn({email: 'c2ji@ucsd.edu', password: 'test'},
        function(newUser) {
          user = newUser;
          inject(function ($controller, $rootScope) {
            scope = $rootScope.$new();
            PackageCtrl = $controller('PackageCtrl', {
              $scope: scope
            });
          });
          done()
        },
        function(error){done()});
    });

    it("Test the Controller", function(){
      expect(Parse.User.current().attributes.name).toEqual(user.attributes.name);
      expect(Parse.User.current().id).toEqual(user.id);
      expect(null).toBe(null);
    });

    it("Test the Interactor",function(){
      try {
        scope.ctrl.findServices({services:{query:"lalala"}}, {});
      }catch(err){}
      scope.ctrl.setServicesCheck({services:{name:"lalala"}})
      scope.ctrl.isServiceInPackage("lalala",{services:{name:"lalala"}});
      scope.ctrl.createPackage({});
      scope.ctrl.addPackage({});
      scope.ctrl.cancelPackage("lalala");
      scope.ctrl.deletePackage("lalala");
      scope.ctrl.findPackagesCustomers();
      scope.ctrl.addPackageToCustomer("lalala","lalala");
      scope.ctrl.deletePackageToCustomer("lalala","lalala");
    });

    it("Test the Presenter",function(){
      scope.ctrl.presenter.errorWithUserSubscriptions("error");
      scope.ctrl.presenter.errorWithAvailablePackages("error");
      scope.ctrl.presenter.errorWithUserRoles();
      scope.ctrl.presenter.errorCreatePackage("error");
      scope.ctrl.presenter.showPackageSuccessfullyAdded();
      scope.ctrl.presenter.errorAddPackage("error");
      scope.ctrl.presenter.showPackageSuccessfullyCanceled();
      scope.ctrl.presenter.errorCancelPackage("error");
      scope.ctrl.presenter.showPackageSuccessfullyDeleted();
      scope.ctrl.presenter.errorDeletePackage("error");
      scope.ctrl.presenter.errorDeletePackageToCustomer("error");
      scope.ctrl.presenter.errorModifyPackage("error");
      try {
        scope.ctrl.presenter.showPackageCreated({services: {name: "lalala"}});
      }catch(err){}
      try {
        scope.ctrl.presenter.showPackagesCustomers({services: {name: "lalala"}});
      }catch(err){}
      scope.ctrl.presenter.showPackageAddedToCustomer("lalala","lalala")
    });

  });

});
