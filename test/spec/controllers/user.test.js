'use strict';

describe('Controller: UsersCtrl', function () {

  // load the controller's module
  beforeEach(module('connectedWorldwideApp'));

  var UsersCtrl,
    user,
    scope;

  describe('Delete self customer user', function(){
    beforeEach(function(done){
      UserDataStore.logIn({email: 'c2ji@ucsd.edu', password: 'test'},
        function(newUser) {
          user = newUser;
          inject(function ($controller, $rootScope) {
            scope = $rootScope.$new();
            UsersCtrl = $controller('UsersCtrl', {
              $scope: scope
            });
          });
          done()
        },
        function(error){done()});
    });

    it("", function(){
      expect(Parse.User.current().attributes.name).toEqual(user.attributes.name);
      expect(Parse.User.current().id).toEqual(user.id);
    });

    it("Test the Interactor",function(){
      var test = {
        "username": "test",
        "password": "test",
        "email": "test@cw.com",
        "birthday": "2015-01-01",
        "address": "test's home",
        "state": "test's state",
        "zip": "111111",
        "city": "test's city",
        "firstname": "test",
        "lastname": "test"
      };
      scope.ctrl.createCustomer(test);
      scope.ctrl.createCustomerRep(test);
      scope.ctrl.createMarketingRep(test);
      scope.ctrl.isRetail(Parse.User.current());
      scope.ctrl.isCommercial(Parse.User.current());
      scope.ctrl.hasAType(Parse.User.current());
    });
  });

});

