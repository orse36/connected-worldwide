'use strict';

describe('Controller: AccountCtrl', function () {

  // load the controller's module
  beforeEach(module('connectedWorldwideApp'));

  var AccountCtrl,
    user,
    scope;

  describe('Log in customer user', function(){
    beforeEach(function(done){
      UserDataStore.logIn({email: 'c2ji@ucsd.edu', password: 'test'},
        function(newUser) {
          user = newUser;
          inject(function ($controller, $rootScope) {
            scope = $rootScope.$new();
            AccountCtrl = $controller('AccountCtrl', {
              $scope: scope
            });
            done()
          });
        },
        function(error){done()});
    });

    //for controller
    it("Test the Controller", function(){
      expect(Parse.User.current().attributes.name).toEqual(user.attributes.name);
      expect(Parse.User.current().id).toEqual(user.id);

      scope.ctlr.changePassword({password:"test",checkPassword:"test"});
      scope.ctlr.changePassword({password:"test",checkPassword:"lalala"});
      UserDataStore.changePassword("test",function(){},function(){});
      scope.ctlr.changeMaxAccountBalance(1000000);
    });

    it("Test the Interactor",function(){

    });
  });

});
