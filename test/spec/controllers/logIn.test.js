'use strict';


describe('Controller: LogintCtrl', function () {

  // load the controller's module
  beforeEach(module('connectedWorldwideApp'));

  var LogInCtrl,
    user,
    scope;

  describe('LogIn Functionality Fail Test', function(){
    beforeEach(function(done){
      UserDataStore.logIn({email: 'c2ji@ucsd.edu', password: 'fail'},
        function(newUser) {
          user = newUser;
          inject(function ($controller, $rootScope) {
            scope = $rootScope.$new();
            LogInCtrl = $controller('LogInCtrl', {
              $scope: scope
            });
          });
          done()
        },
        function(error){done()});
    });

    it("Test LogIn with wrong informations", function(){
      expect(Parse.User.current()).toEqual(user);
    });
  })

  describe('LogIn Functionality Test', function(){
    beforeEach(function(done){
      UserDataStore.logIn({email: 'c2ji@ucsd.edu', password: 'test'},
        function(newUser) {
          user = newUser;
          inject(function ($controller, $rootScope) {
            scope = $rootScope.$new();
            LogInCtrl = $controller('LogInCtrl', {
              $scope: scope
            });
          });
          done()
        },
        function(error){done()});
    });

    it("Test the Controller", function(){
      expect(Parse.User.current().attributes.name).toEqual(user.attributes.name);
      expect(Parse.User.current().id).toEqual(user.id);
    });

    it("Test the Interactor",function(){
      scope.ctrl.logIn({email: 'c2ji@ucsd.edu', password: 'test'});
      scope.ctrl.logIn({email: 'c2ji@ucsd.edu', password: 'lalala'});
      scope.ctrl.requestPassword('lalala@ucsd.edu');
    });
  });

});
