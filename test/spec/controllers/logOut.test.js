'use strict';

 describe('Controller: LogOutCtrl', function () {

   // load the controller's module
   beforeEach(module('connectedWorldwideApp'));

   var LogOutCtrl,
     user,
     scope;

   describe('Try the logout controller', function(){
     beforeEach(function(done){
       UserDataStore.logIn({email: 'c2ji@ucsd.edu', password: 'test'},
         function(newUser) {
           user = newUser;
           inject(function ($controller, $rootScope) {
             scope = $rootScope.$new();
             LogOutCtrl = $controller('LogOutCtrl', {
               $scope: scope
             });
           });
           done()
         },
         function(error){done()});
     });

     it("", function(){
       expect(Parse.User.current()).toBe(null);
     });

     it("Test the Interactor",function(){

     });
   });

 });
