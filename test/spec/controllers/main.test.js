'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('connectedWorldwideApp'));

  var MainCtrl,
    user,
    scope;

  describe('Delete self customer user', function(){
    beforeEach(function(done){
      UserDataStore.logIn({email: 'c2ji@ucsd.edu', password: 'test'},
        function(newUser) {
          user = newUser;
          inject(function ($controller, $rootScope) {
            scope = $rootScope.$new();
            MainCtrl = $controller('MainCtrl', {
              $scope: scope
            });
          });
          done()
        },
        function(error){done()});
    });

    it("", function(){
      expect(Parse.User.current().attributes.name).toEqual(user.attributes.name);
      expect(Parse.User.current().id).toEqual(user.id);
      expect(null).toBe(null);
    });

    it("Test the Interactor",function(){
      scope.ctrl.userLogged();
      scope.ctrl.isCustomer();
    });
  });

});
