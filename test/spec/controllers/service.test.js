'use strict';

describe('Controller: ServiceCtrl', function () {

  // load the controller's module
  beforeEach(module('connectedWorldwideApp'));

  var ServiceCtrl,
    user,
    scope;

  describe('Delete self customer user', function(){
    beforeEach(function(done){
      UserDataStore.logIn({email: 'c2ji@ucsd.edu', password: 'test'},
        function(newUser) {
          user = newUser;
          inject(function ($controller, $rootScope) {
            scope = $rootScope.$new();
            ServiceCtrl = $controller('ServiceCtrl', {
              $scope: scope
            });
          });
          done()
        },
        function(error){done()});
    });

    it("", function(){
      expect(Parse.User.current().attributes.name).toEqual(user.attributes.name);
      expect(Parse.User.current().id).toEqual(user.id);
      expect(null).toBe(null);
    });

    it("Test the Interactor",function(){
      scope.ctrl.cancelService("Space");
      scope.ctrl.addService("Space");
      scope.ctrl.findServicesCustomers();
      scope.ctrl.deleteServiceToCustomer("Space","c2ji@ucsd.edu");
      scope.ctrl.addServiceToCustomer("Space","c2ji@ucsd.edu");
      scope.ctrl.createService({sName: "lalala", sDuration: 3, sDescription: "la", sPrice: 100});
      scope.ctrl.deleteService("lalala");
    });

    it("Test the Presenter",function(){
      scope.ctrl.presenter.errorWithUserSubscriptions("error");
      scope.ctrl.presenter.errorWithAvailableServices("error");
      scope.ctrl.presenter.errorWithUserRoles();
      scope.ctrl.presenter.errorAddService("error");
      scope.ctrl.presenter.showServiceSuccessfullyCanceled();
      scope.ctrl.presenter.showServiceSuccessfullyDeleted();
      scope.ctrl.presenter.showServiceAddedToCustomer("lalala","lalala");
      scope.ctrl.presenter.showServiceDeletedFromCustomer("lalala","lalala");
      scope.ctrl.presenter.showServiceSuccessfullyAdded();
      scope.ctrl.presenter.errorCancelService("error");
    });
  });

});
